import gzip
from slpp import slpp

from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseBadRequest
from django.shortcuts import *
from django.views.decorators.csrf import csrf_exempt

from core.uploadhandler import UploadHandlerAPI
from thing.views.upload import MAX_UPLOAD_SIZE

# ---------------------------------------------------------------------------

class UploadForm(forms.Form):
    username = forms.CharField(max_length=32, required=False)
    password = forms.CharField(max_length=32, required=False)
    api_key = forms.CharField(max_length=64, required=False)
    compressed = forms.CharField(max_length=4, required=False)
    lua_file = forms.FileField()

    def clean(self):
        cleaned = super().clean()

        username = cleaned.get('username')
        password = cleaned.get('password')
        api_key = cleaned.get('api_key')
        if not (api_key or (username and password)):
            self.add_error('username', 'Must provide username/password pair or api_key')
            self.add_error('password', 'Must provide username/password pair or api_key')
            self.add_error('api_key', 'Must provide username/password pair or api_key')

    def clean_lua_file(self):
        lua_file = self.cleaned_data['lua_file']
        if lua_file.size > MAX_UPLOAD_SIZE:
            raise forms.ValidationError("File is larger than the %sKiB limit!" % (MAX_UPLOAD_SIZE / 1024))

        data = lua_file.read()

        if self.cleaned_data['compressed'] == 'yup':
            try:
                temp_data = gzip.decompress(data)
                data = temp_data
            except Exception as e:
                raise forms.ValidationError("Error decompressing data!")

        data = data.decode('utf-8').replace('WWTCSaved = {', '{')
        try:
            parsed = slpp.decode(data)
        except:
            raise forms.ValidationError("File does not contain valid SavedVariables data!")
        else:
            return parsed

# ---------------------------------------------------------------------------

@csrf_exempt
def upload(request):
    if request.method == 'POST':
        form = UploadForm(request.POST, request.FILES)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            api_key = form.cleaned_data.get('api_key')
            user = None

            if username and password:
                user = authenticate(username=username, password=password)
                if user is None:
                    raise PermissionDenied()

            elif api_key:
                try:
                    user = User.objects.get(profile__api_key=api_key)
                except User.DoesNotExist:
                    raise PermissionDenied()

            request.user = user
            uh = UploadHandlerAPI(request, form.cleaned_data)
            return uh.run()

        else:
            return HttpResponseBadRequest("Invalid form data")

    return HttpResponseBadRequest("Invalid request type")

# ---------------------------------------------------------------------------
