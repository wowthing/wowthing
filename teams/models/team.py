import random

from django.contrib.auth.models import User
from django.db import models

chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890'

# ---------------------------------------------------------------------------

class Team(models.Model):
    owner = models.ForeignKey(User, related_name='teams', on_delete=models.CASCADE)

    name = models.CharField(max_length=64)
    description = models.TextField()
    region = models.CharField(max_length=2)
    access_key = models.CharField(max_length=16)
    default_realm = models.ForeignKey('core.Realm', blank=True, null=True, on_delete=models.SET_NULL)

    def generate_access_key(self):
        self.access_key = ''.join(random.choice(chars) for i in range(16))

# ---------------------------------------------------------------------------
