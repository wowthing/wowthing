from django.conf.urls import include, url

from . import views

app_name = 'teams'
urlpatterns = [
    url(r'^create/$', views.create, name='create'),

    url(r'^add_character/$', views.add_character, name='add_character'),
    url(r'^del_character/(?P<team_id>\d+)/(?P<member_id>\d+)/$', views.del_character, name='del_character'),
    url(r'^edit_roles/(?P<team_id>\d+)/$', views.edit_roles, name='edit_roles'),

    url(r'^(?P<key>[A-Za-z0-9]{,16})/$', views.view, name='view'),
]
