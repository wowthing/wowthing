from django.contrib.auth.models import User
from django.db import models

# ---------------------------------------------------------------------------

class CharacterTag(models.Model):
    id = models.AutoField(primary_key=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    short_name = models.CharField(max_length=8)
    long_name = models.CharField(max_length=64)
    foreground_colour = models.CharField(max_length=6)
    background_colour = models.CharField(max_length=6)

# ---------------------------------------------------------------------------
