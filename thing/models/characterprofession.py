from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class CharacterProfession(models.Model):
    PRIMARY_SKILL = 1
    SECONDARY_SKILL = 2
    SKILL_CHOICES = (
        (PRIMARY_SKILL, "Primary"),
        (SECONDARY_SKILL, "Secondary"),
    )

    character = models.ForeignKey('Character', related_name='professions', on_delete=models.CASCADE)
    profession = models.ForeignKey('core.Profession', on_delete=models.CASCADE)

    skill_type = models.SmallIntegerField(choices=SKILL_CHOICES)
    current_skill = models.SmallIntegerField()
    max_skill = models.SmallIntegerField()

    recipes = ArrayField(models.IntegerField(), default=list)

    class Meta:
        ordering = ['skill_type', 'id']

# ---------------------------------------------------------------------------
