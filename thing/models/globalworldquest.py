from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class GlobalWorldQuest(models.Model):
    region = models.CharField(max_length=2, primary_key=True)

    factions = ArrayField(models.IntegerField(), default=list)
    expires = ArrayField(models.IntegerField(), default=list)

# ---------------------------------------------------------------------------
