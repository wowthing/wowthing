# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2019-02-15 02:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thing', '0021_characteritem_context'),
    ]

    operations = [
        migrations.AddField(
            model_name='character',
            name='keystone_week_end',
            field=models.IntegerField(default=0),
        ),
    ]
