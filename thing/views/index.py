from django.shortcuts import *

# ---------------------------------------------------------------------------

def index(request):
    return render(
        request,
        'thing/index.html',
    )

# ---------------------------------------------------------------------------
