from django.shortcuts import *

# ---------------------------------------------------------------------------

def leaderboards(request):
    return render(
        request,
        'thing/leaderboards.html',
    )

# ---------------------------------------------------------------------------
