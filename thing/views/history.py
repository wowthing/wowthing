import os

from django.shortcuts import *

# ---------------------------------------------------------------------------

VERSION_FILE = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '../../version.txt'))

# ---------------------------------------------------------------------------

def history(request):
    versions = []
    header = None
    date = None
    entries = []
    for line in open(VERSION_FILE):
        line = line.strip()
        if header is None:
            header, date = line.rsplit(' - ', 1)
        elif line.startswith('---'):
            pass
        elif line == '':
            versions.append([header, date, entries])
            header = None
            date = None
            entries = []
        else:
            if line.startswith('*'):
                entries.append([line[2:], []])
            elif line.startswith('-'):
                entries[-1][1].append(line[2:])

    if header and entries:
        versions.append([header, date, entries])

    return render(
        request,
        'thing/history.html',
        dict(
            versions=versions,
        ),
    )

# ---------------------------------------------------------------------------
