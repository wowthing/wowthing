from django.db import models

# ---------------------------------------------------------------------------

class BigAutoField(models.AutoField):
    def db_type(self, connection):  # pylint: disable=W0621
        if 'postgres' in connection.settings_dict['ENGINE']:
            return 'bigserial'
        return super(BigAutoField, self).db_type(connection)

# ---------------------------------------------------------------------------
