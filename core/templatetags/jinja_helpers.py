#import jinja2
import re

from django.contrib.staticfiles.storage import staticfiles_storage
from django.template.defaultfilters import stringfilter
#from django_jinja import library
#from jingo import register

# Put commas in things
# http://code.activestate.com/recipes/498181-add-thousands-separator-commas-to-formatted-number/
# re_digits_nondigits = re.compile(r'\d+|\D+')

# @library.filter
# @stringfilter
# def commas(value):
#     parts = re_digits_nondigits.findall(value)
#     for i in xrange(len(parts)):
#         s = parts[i]
#         if s.isdigit():
#             parts[i] = _commafy(s)
#             break
#     return ''.join(parts)

# def _commafy(s):
#     r = []
#     for i, c in enumerate(reversed(s)):
#         if i and (not (i % 3)):
#             r.insert(0, ',')
#         r.insert(0, c)
#     return ''.join(r)

# # Jinja2 filter version of staticfiles. Hopefully.
# #@register.function
# #def static(path):
# #    return staticfiles_storage.url(path)


# @library.filter
# def bsfield(field):
#     if field.field.__class__.__name__ == 'BooleanField':
#         html = """
# <div class="checkbox">
#   <label>%s %s</label>
# </div>
# """ % (str(field), field.label)
#     else:
#         html = ""

#     return jinja2.Markup(html)

# @library.filter
# def bshfield(field, cols=10):
#     print field.field.__class__.__name__
#     if field.field.__class__.__name__ == 'BooleanField':
#         html = """
# <div class="form-group">
#   <div class="col-sm-offset-2 col-sm-%s">
#     <div class="checkbox">
#       <label>%s %s</label>
#     </div>
#   </div>
# </div>
# """ % (cols, str(field), field.label)

#     else:
#         html = """
# <div class="form-group">
#   <label for="%s" class="col-sm-2 control-label">%s</label>
#   <div class="col-sm-%s">%s</div>
# </div>""" % (field.auto_id, field.label, cols, str(field))

#     return jinja2.Markup(html)

#               # <div class="form-group">
#               #   <label for="{{ field.auto_id }}" class="col-sm-2 control-label">{{ field.label }}</label>
#               #   <div class="col-sm-2">
#               #     {{ field|safe }}
#               #   </div>
#               # </div>


# # Turn a duration in seconds into a human readable string
# @library.filter
# def duration(s):
#     m, s = divmod(s, 60)
#     h, m = divmod(m, 60)
#     d, h = divmod(h, 24)

#     parts = []
#     if d:
#         parts.append('%dd' % (d))
#     if h:
#         parts.append('%dh' % (h))
#     if m:
#         parts.append('%dm' % (m))
#     if s:
#         parts.append('%ds' % (s))

#     return ' '.join(parts[:2])
