import json

from django.core.management.base import BaseCommand, CommandError

import data.manual as manual_data
from core.models import *
from thing.models import CharacterLockout

# ---------------------------------------------------------------------------

DATA_KEYS = [
    'achievement_reps',
    'bag_data',
    'bag_slots',
    'base_item_level',
    'bodyguard_factions',
    'currency_categories',
    'currency_highlights',
    'current_expansion',
    'difficulty_short',
    'emissary_alliance',
    'enchant_slots',
    'expansions',
    'faction_icons',
    'honor_per_level',
    'keystone_maps',
    'keystone_order',
    'max_player_level',
    'mythic_dungeons',
    'paragon_rewards',
    'profession_cooldowns',
    'profession_map',
    'progress',
    'progress_data',
    'raid_order_override',
    'region_armory',
    'reputation_levels',
    'reputations',
    'slot_names',
    'slot_order',
    'socket_bonusids',
    'sub_professions',
    'wowdb_base',
    'wowhead_base',
]

SKIP_ACHIEVEMENTS = set([
    7268, # The Temple of Kotmogu
    7270, # For Display Only
    7269, # Stay Off the Grass
    10077,# Mythic Dungeoneer
])

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        self.lines = []
        self.lines.append('var WoWthing = WoWthing || {};')
        # self.lines.append('var f=WoWthing.Follower,m=WoWthing.Mission;')

        all_data = {}

        # wowthing-data things
        for key in DATA_KEYS:
            all_data[key] = getattr(manual_data, key)

        # Other junk
        all_data['difficulties'] = dict(CharacterLockout.DIFFICULTY_CHOICES)
        all_data['split_lockouts'] = {sl: 1 for sl in manual_data.split_lockouts}

        # Mounts
        mounts = []
        for set_name, mount_ids in manual_data.mounts:
            for mount_id in mount_ids:
                if isinstance(mount_id, int):
                    mounts.append(mount_id)
                else:
                    mounts.extend(mount_id)
        mount_map = Spell.objects.in_bulk(mounts)

        all_data['mounts'] = self.add_all_sets(manual_data.mounts, mount_map)

        # Pets
        pets = []
        for set_name, pet_ids in manual_data.pets:
            pets.extend(pet_ids)
        pet_map = BattlePet.objects.in_bulk(pets)

        all_data['pets'] = self.add_all_sets(manual_data.pets, pet_map)

        # Toys
        toys = []
        for set_name, toy_ids in manual_data.toys:
            for toy_id in toy_ids:
                if isinstance(toy_id, int):
                    toys.append(toy_id)
                else:
                    toys.extend(toy_id)
        toy_map = Item.objects.in_bulk(toys)

        all_data['toys'] = self.add_all_sets(manual_data.toys, toy_map)

        # # Abilities
        # self.lines.append('abilities: {')
        # for ability in GarrisonAbility.objects.all():
        #     self.lines.append('%s: {id: %s, name: "%s", effect: "%s", icon: "%s", quality: %s},' % (ability.id, ability.id, ability.name, ability.effect, ability.icon, ability.quality))
        # self.lines.append('},')
        #
        # # Followers
        # self.lines.append('followers: {')
        # for follower in GarrisonFollower.objects.all():
        #     self.lines.append('%s: {id: %s, an: "%s", hn: "%s", ap: "%s", hp: "%s"},' % (follower.id, follower.id, follower.alliance_name, follower.horde_name,
        #         follower.alliance_portrait, follower.horde_portrait))
        # self.lines.append('},')
        #
        # # Garrison talents
        # talents = {}
        # for talent in GarrisonTalent.objects.all().order_by('class_id', 'tier', 'column'):
        #     talents.setdefault(talent.class_id, {}).setdefault(talent.tier, []).append(dict(id=talent.id, name=talent.name, icon=talent.icon))
        # self.lines.append('garrison_talents: %s,' % (json_dump(talents)))

        # Data stuff
        # self.lines.append('artifact_tier_1_limit: %d,' % (data.artifact_tier_1_limit))
        # self.lines.append('artifact_xp_1: %s,' % (json_dump(data.artifact_xp_1)))
        # self.lines.append('artifact_xp_2: %s,' % (json_dump(data.artifact_xp_2)))
        # self.lines.append('artifacts: %s,' % (json_dump(data.artifacts)))
        # self.lines.append('crucible_levels: %s,' % (json_dump(data.crucible_levels)))
        # self.lines.append('followers_available: %s,' % (json_dump(data.followers_available)))
        # self.lines.append('followers_optimization: %s,' % (json_dump(data.followers_optimization)))
        # self.lines.append('followers_override: %s,' % (json_dump(data.followers_override)))

        # Done
        self.lines.append('Object.assign(WoWthing.data, %s);' % (json_dump(all_data)))

        open('assets/generated/data.js', 'w').write('\n'.join(self.lines))


        # Achievements
        self.lines = []
        self.lines.append('var a=WoWthing.Achievement,c=WoWthing.AchievementCriteria;')
        self.lines.append('WoWthing.data.achievements=[')
        self.achievements()
        self.lines.append('],')

        self.achievement_data()

        open('assets/generated/achievements.js', 'w').write('\n'.join(self.lines))

        # Transmogs
        self.lines = []

        self.transmog_data()

        open('assets/generated/transmog.js', 'w').write('\n'.join(self.lines))

    def add_all_sets(self, thing_sets, thing_map, include_name=False):
        things = []
        for set_name, thing_ids in thing_sets:
            thing = [set_name, []]
            for thing_id in thing_ids:
                if isinstance(thing_id, int):
                    thing[1].append(self.add_thing(thing_map, thing_id, include_name))
                else:
                    more_things = []
                    for t_id in thing_id:
                        more_things.append(self.add_thing(thing_map, t_id, include_name))
                    thing[1].append(more_things)
            things.append(thing)

        return things

    def add_thing(self, thing_map, thing_id, include_name=False):
        if thing_id is None:
            return None#self.lines.append('null,')
        else:
            item = thing_map.get(thing_id)
            if item is None:
                print('Missing thing: %s', thing_id)
            else:
                if isinstance(item, Item):
                    if include_name:
                        #self.lines.append('{id: %d, icon: "%s", quality: %s, name: "%s"},' % (item.id, item.icon, item.quality, item.name.replace('"', '\\"')))
                        return dict(id=item.id, icon=item.icon, quality=item.quality, name=item.name)
                    else:
                        #self.lines.append('{id: %d, icon: "%s", quality: %s},' % (item.id, item.icon, item.quality))
                        return dict(id=item.id, icon=item.icon, quality=item.quality)
                elif isinstance(item, BattlePet):
                    #self.lines.append('{id: %d, icon: "%s"},' % (item.id, item.icon))
                    return dict(id=item.id, icon=item.icon)
                elif isinstance(item, Spell):
                    #self.lines.append('{id: %d, icon: "%s", name: "%s"},' % (item.id, item.icon, item.name.replace('"', '\\"')))
                    return dict(id=item.id, icon=item.icon, name=item.name)
                else:
                    print('Weird object: %r', item)

    # -----------------------------------------------------------------------

    def achievements(self):
        self.adata = json.load(open('data/raw/achievement_data.json'))
        #print self.adata['achievements']

        self.temp = open('a.lua', 'w')
        self.temp.write('allAchievements = {')

        self.criteria_map = {}

        achievements = json.load(open('data/raw/achievement.json'))['achievements']

        self.achievement_titles = {}
        for category in achievements:
            self.make_achievement_titles(category)

        for category in achievements:
            self.achievement_category(category)

        self.temp.write('}\n')

        open('data/raw/criteria_map.json', 'w').write(json_dump(self.criteria_map))

    def make_achievement_titles(self, category):
        for achievement in category.get('achievements', []):
            self.achievement_titles[achievement['id']] = achievement['title']

        for sub_category in category.get('categories', []):
            self.make_achievement_titles(sub_category)

    def achievement_category(self, category):
        self.lines.append('{id:%(id)d,name:"%(name)s",achievements:[' % (category))

        for achievement in category.get('achievements', []):
            if achievement['id'] in SKIP_ACHIEVEMENTS:
                continue

            self.temp.write('%(id)d,' % achievement)

            parts = []
            # parts.append('i:%(id)d' % achievement)
            # parts.append('t:"%s"' % achievement['title'].replace('"', '\\"'))
            # parts.append('d:"%s"' % achievement['description'].replace('"', '\\"').strip())
            # parts.append('n:"%(icon)s"' % achievement)
            # parts.append('p:%(points)d' % achievement)
            # parts.append('f:%(factionId)d' % achievement)
            # parts.append('a:%d' % (achievement['accountWide'] and 1 or 0))

            parts.append(achievement['id'])
            parts.append(achievement['title'])
            parts.append(achievement['description'].strip())
            parts.append(achievement['icon'].replace('__', '_'))
            parts.append(achievement['points'])
            parts.append(achievement['factionId'])
            parts.append(achievement['accountWide'] and 1 or 0)

            achievement_criteria = self.adata['achievements'].get(str(achievement['id']), None)
            #if achievement['criteria']:
            if achievement_criteria and achievement['criteria']:
                parts.append(achievement_criteria.pop(0))
                # achievement['criteria'].sort(key=lambda x: x['orderIndex'])

                criteria = []
                #for c in achievement['criteria']:
                for (criteriaID, requiredQuantity) in achievement_criteria:
                    if criteriaID > 0:
                        criteria.append([
                            criteriaID,
                            requiredQuantity,
                            #c['id'],
                            #c.get('max', 0),
                        ])

                #parts.append('c:%r' % criteria)
                parts.append(criteria)

                for c in criteria:
                    self.criteria_map.setdefault(c[0], []).append(achievement['id'])

            # reward = achievement.get('reward')
            # if reward is not None:
            #     parts.append('r:"%s"' % reward)

            # if achievement['rewardItems']:
            #     parts.append('ri:%(id)d' % achievement['rewardItems'][0])

            #self.lines.append('new a({%s}),' % ','.join(parts))
            self.lines.append('new a(%s),' % (json_dump(parts)[1:-1]))

        self.lines.append('],')

        categories = category.get('categories', [])
        if categories:
            self.lines.append('categories:[')
            for cat in categories:
                self.achievement_category(cat)
            self.lines.append('],')
        self.lines.append('},')

    # -----------------------------------------------------------------------

    def achievement_data(self):
        self.lines.append('WoWthing.data.achievement_next=%s;' % json_dump(self.adata['chains']))

        prev = dict((v, int(k)) for k, v in self.adata['chains'].items())
        self.lines.append('WoWthing.data.achievement_prev=%s;' % json_dump(prev))

        self.lines.append('WoWthing.data.achievement_criteria={')
        seen = {}
        for criterias in self.adata['criteria'].values():
            for criteria in criterias:
                if criteria['criteriaID'] == 0:
                    continue

                #if criteria['criteriaID'] in seen:
                #    print 'duplicate?', criteria
                #    continue
                seen[criteria['criteriaID']] = True

                description = criteria['description']
                if criteria['type'] == 8:
                    ach_desc = self.achievement_titles.get(criteria['assetID'])
                    if ach_desc and ach_desc != description:
                        #print criteria, ach_desc
                        description = ach_desc

                # if criteria['description'] == 'Explore Frostfire Ridge':
                #     print criteria

                parts = [
                    criteria['assetID'],
                    description,
                    criteria['type'],
                ]

                flags = criteria.get('flags')
                if flags:
                    parts.append(flags)

                self.lines.append('"%d":new c(%s),' % (criteria['criteriaID'], json_dump(parts)[1:-1]))

        self.lines.append('};')

    # -----------------------------------------------------------------------

    def transmog_data(self):
        raw_transmog = json.load(open('data/raw/transmog.json'))
        transmog = {}
        for cls, subclasses in raw_transmog.items():
            transmog[cls] = {}
            for subclass, transmogs in subclasses.items():
                transmog[cls][subclass] = [(t['n'], t['i']) for t in transmogs]

        self.lines.append('WoWthing.data.transmog=%s;' % json.dumps(transmog))
        
        raw_rewards = json.load(open('data/raw/quest_reward.json'))
        rewards = {}
        for thing in raw_rewards:
            if thing['i'] not in rewards:
                rewards[thing['i']] = []
            rewards[thing['i']].append(thing['q'])

        self.lines.append('WoWthing.data.transmog_quest=%s;' % json.dumps(rewards))

# ---------------------------------------------------------------------------

def json_dump(obj):
    return json.dumps(obj, separators=(',', ':'))

# ---------------------------------------------------------------------------
