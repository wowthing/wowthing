import re
import requests
import sys

from django.core.management.base import BaseCommand, CommandError

from core.models import GarrisonAbility, GarrisonFollower

# ---------------------------------------------------------------------------

FOLLOWERS_URL = "http://www.wowdb.com/garrison/followers/order-hall?page=%d&sort=-level"

FOLLOWER_TBODY_RE = re.compile(r'<tbody>(.*?)</tbody>', re.S)
FOLLOWER_TR_RE = re.compile(r'<tr[^>]+>(.*?)</tr>', re.S)
FOLLOWER_DATA_RE = re.compile(r'src=".*?/\d+/(.*?)\.jpg".*?/followers/(\d+)-.*?>(.*?)<.*?src=".*?/\d+/(.*?)\.jpg".*?/followers/\d+-.*?>(.*?)<', re.S)

EQUIPMENT_ABILITIES_URL = 'http://www.wowdb.com/garrison/abilities/order-hall/traits?page=%d'
EQUIPMENT_ITEMS_URL = 'http://www.wowdb.com/items/consumables/other?page=%d&filter-champion-equipment=1'

ABILITIES_DATA_RE = re.compile(r'<a class="listing-icon".*?/abilities/(\d+).*?>.*?<img.*?/medium/([^>]+)\.jpg".*?<a class="t".*?>(.*?)</a>.*?<td class="col-description">(.*?)</td>', re.S)
ITEMS_DATA_RE = re.compile(r'class="q(\d) t">(.*?)</a>', re.S)

# ---------------------------------------------------------------------------

NAME_FIXES = {
    'Vilefiend-Spine Whip': 'Vilefiend Spine-Whip',
    'Pulsing Wrathguard Skull': 'Pulsating Wrathguard Skull',
}

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        self.update_equipment()
        self.update_followers()

    def update_equipment(self):
        print 'Fetching equipment...',
        sys.stdout.flush()

        abilities = {}

        for i in range(2):
            r = requests.get(EQUIPMENT_ABILITIES_URL % (i + 1))
            if r.status_code == requests.codes.ok:
                datas = ABILITIES_DATA_RE.findall(r.text)
                if datas:
                    print 'done.'

                    for data in datas:
                        name = data[2].replace('&#x27;', "'")
                        name = NAME_FIXES.get(name, name)
                        effect = data[3].strip().replace('&#x27;', "'")

                        if name not in abilities:
                            abilities[name] = []

                        abilities[name].append(dict(
                            id=int(data[0]),
                            name=name,
                            effect=effect,
                            icon=data[1],
                            quality=1,
                        ))
                else:
                    print 'fail (re search).'
            else:
                print 'fail (%s).' % (r.status_code)

        for i in range(2):
            r = requests.get(EQUIPMENT_ITEMS_URL % (i + 1))
            if r.status_code == requests.codes.ok:
                datas = ITEMS_DATA_RE.findall(r.text)
                if datas:
                    print 'done.'

                    for data in datas:
                        name = data[1].replace('&#x27;', "'")
                        name = NAME_FIXES.get(name, name)

                        if name in abilities:
                            for ability in abilities[name]:
                                ability['quality'] = int(data[0])
                        else:
                            print 'missing ability for %r' % (name)
                else:
                    print 'fail (re search).'
            else:
                print 'fail (%s).' % (r.status_code)

        ability_map = {}
        for ability in GarrisonAbility.objects.all():
            ability_map[ability.id] = ability

        new = []
        for sigh in abilities.values():
            for data in sigh:
                ability = ability_map.get(data['id'])
                if ability is None:
                    new.append(GarrisonAbility(
                        id=data['id'],
                        name=data['name'],
                        effect=data['effect'],
                        icon=data['icon'],
                        quality=data['quality'],
                    ))
                elif ability.name != data['name'] or ability.effect != data['effect'] or ability.icon != data['icon'] or ability.quality != data['quality']:
                    ability.name = data['name']
                    ability.effect = data['effect']
                    ability.icon = data['icon']
                    ability.quality = data['quality']
                    ability.save()

        GarrisonAbility.objects.bulk_create(new)

    # -----------------------------------------------------------------------

    def update_followers(self):
        print 'Fetching followers...',
        sys.stdout.flush()

        for i in range(5):
            r = requests.get(FOLLOWERS_URL % (i + 1))
            if r.status_code == requests.codes.ok:
                m = FOLLOWER_TBODY_RE.search(r.text)
                if m:
                    print 'done.'

                    follower_map = {}
                    for follower in GarrisonFollower.objects.all():
                        follower_map[follower.id] = follower
                    
                    new = []

                    trs = FOLLOWER_TR_RE.findall(m.group(1))
                    for tr in trs:
                        m2 = FOLLOWER_DATA_RE.search(tr)
                        if m2:
                            id = int(m2.group(2))
                            alliance_portrait = m2.group(1)
                            alliance_name = m2.group(3)
                            horde_portrait = m2.group(4)
                            horde_name = m2.group(5)

                            follower = follower_map.get(id)
                            if follower is None:
                                new.append(GarrisonFollower(
                                    id=id,
                                    alliance_name=alliance_name,
                                    alliance_portrait=alliance_portrait,
                                    horde_name=horde_name,
                                    horde_portrait=horde_portrait,
                                ))
                            elif follower.alliance_name != alliance_name or follower.alliance_portrait != alliance_portrait or \
                                follower.horde_name != horde_name or follower.horde_portrait != horde_portrait:
                                follower.alliance_name = alliance_name
                                follower.alliance_portrait = alliance_portrait
                                follower.horde_name = horde_name
                                follower.horde_portrait = horde_portrait
                                follower.save()

                    GarrisonFollower.objects.bulk_create(new)

                else:
                    print 'fail (re search).'
            else:
                print 'fail (%s).' % (r.status_code)

# ---------------------------------------------------------------------------
