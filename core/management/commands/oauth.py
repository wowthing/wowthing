import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q

from core.models import BNetAccount
from django.utils import timezone
from thing.views.oauth import _update_oauth

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        now = timezone.now()
        one_hour_ago = now - datetime.timedelta(hours=1)

        bna_qs = BNetAccount.objects.filter(
            Q(last_update__isnull=True) | Q(last_update__lt=one_hour_ago),
            token_expires__gt=now,
        )

        update_ids = []
        for bna in bna_qs:
            _update_oauth(bna)
            update_ids.append(bna.id)

        if update_ids:
            BNetAccount.objects.filter(pk__in=update_ids).update(last_update=now)

# ---------------------------------------------------------------------------
