from django.core.management.base import BaseCommand, CommandError
from core.models import BattlePet

pets = [24480, 25706, 32592, 54539, 62121, 63365, 71942, 72462, 72463, 72464, 73688, 86715, 86718]

class Command(BaseCommand):
    def handle(self, *args, **options):
        f = open('/home/freddie/public_html/blah.html', 'w')
        f.write("""
<html>
<head>
<title>Missing pets</title>
</head>
<body>
<h2>Missing pets: %d</h2>
""" % len(pets))

        pet_map = BattlePet.objects.in_bulk(pets)
        for petid in pets:
            f.write('<a href="http://www.wowhead.com/npc=%d">%05d. %s</a><br>' % (petid, petid, pet_map[petid].name))

        f.write("""
        <script type="text/javascript" src="//static.wowhead.com/widgets/power.js" async></script>
        </body>
        </html>
        """)
