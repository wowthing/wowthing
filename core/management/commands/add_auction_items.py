from django.core.management.base import BaseCommand, CommandError
from django.db import connection, transaction

# ---------------------------------------------------------------------------

ITEM_QUERY = """
INSERT INTO core_item
(id, name, lname, icon, bind_type, cls, level, level_scaling, quality, sell_price, slot, subcls, upgrades)
SELECT DISTINCT a.item_id, '###UPDATE###', '', '', 0, 0, 0, '{}'::integer[], 0, 0, 0, 0, '{}'::integer[]
FROM auctions_auction a
WHERE NOT EXISTS (
    SELECT 1
    FROM core_item i2
    WHERE i2.id = a.item_id
)
"""

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        with transaction.atomic():
            # Lock the table :siren:
            cursor = connection.cursor()
            cursor.execute('LOCK TABLE core_item')

            cursor.execute(ITEM_QUERY)

# ---------------------------------------------------------------------------
