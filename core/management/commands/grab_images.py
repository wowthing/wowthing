import os
import requests
import sys

from django.core.management.base import BaseCommand, CommandError
from django.db import connection
from PIL import Image

import core.data
from core.models import CharacterRace

# ---------------------------------------------------------------------------

BLIZZARD_ICON_URL = 'http://media.blizzard.com/wow/icons/%s/%s.jpg'
PORTRAIT_URL = 'http://wow.zamimg.com/images/wow/garr/%s.png'
WOWDB_ICON_URL = 'http://media-azeroth.cursecdn.com/wow/icons/%s/%s.jpg'

WOWDB_SIZE_MAP = {
    18: 'small',
    36: 'medium',
    56: 'large',
}

ICON_QUERY = """
SELECT DISTINCT icon
FROM (
    SELECT icon FROM core_battlepet
    UNION
    SELECT icon FROM core_characterclass
    UNION
    SELECT icon FROM core_characterspecialization
    UNION
    SELECT icon FROM core_currency
    UNION
    --SELECT icon FROM core_item
    --UNION
    SELECT icon FROM core_profession
    UNION
    SELECT icon FROM core_spell
) blah
WHERE NOT icon = ''
"""

ICON_PATH = 'img/%s/%s.jpg'
ICON_SIZES = (
    18,
    36,
    # 56,
)
ICON_EXTRA = (
    'inv_misc_coin_02',
    'ability_dualwield',
)

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        icons = set(ICON_EXTRA)

        with connection.cursor() as c:
            c.execute(ICON_QUERY)
            for row in c:
                icons.add(row[0])

        for raceID in CharacterRace.objects.values_list('id', flat=True):
            icons.add('race_%s_0' % (raceID))
            icons.add('race_%s_1' % (raceID))

        for currency in core.data.currency_items:
            icons.add(currency['icon'])

        for spells in core.data.profession_cooldowns.values():
            for spell in spells:
                icons.add(spell['icon'])

        #for group in core.data.reputations:
        #    for rep in group['reputations']:
        #        icons.add(rep['icon'])

        #for group in core.data.progress:
        #    icons.add(group['icon'])

        # achievements aaa
        for cat in core.data.achievements:
            self.do_category(icons, cat)

        failed = []

        for icon in sorted(icons):
            for size in ICON_SIZES:
                if icon.startswith('race_') and size != 18:
                    continue

                iconpath = ICON_PATH % (size, icon)
                if not os.path.exists(iconpath):
                    iconugh = icon.replace(' ', '_').replace('-', '_')
                    #url = BLIZZARD_ICON_URL % (size, iconugh)
                    url = WOWDB_ICON_URL % (WOWDB_SIZE_MAP[size], iconugh)

                    print 'Fetching %s...' % (url),
                    sys.stdout.flush()

                    r = requests.get(url)
                    if r.status_code == requests.codes.ok:
                        print 'done.'

                        open(iconpath, 'wb').write(r.content)
                    else:
                        print 'fail (%s).' % (r.status_code)

                        # if size == 18:
                        #     failed.append(icon)
                        # else:
                        #     url = WOWDB_ICON_URL % (WOWDB_SIZE_MAP[size], iconugh)

                        #     print 'Fetching %s...' % (url),
                        #     sys.stdout.flush()

                        #     r = requests.get(url)
                        #     if r.status_code == requests.codes.ok:
                        #         print 'done.'

                        #         open(iconpath, 'wb').write(r.content)
                        #     else:
                        #         print 'fail (%s).' % (r.status_code)

        # Now resize failed ones
        for icon in failed:
            in_path = ICON_PATH % (36, icon)
            if os.path.exists(in_path):
                out_path = ICON_PATH % (18, icon)

                im = Image.open(in_path)
                im.thumbnail((18, 18), Image.ANTIALIAS)
                im.save(out_path)

                print 'Resized %s' % (icon)

            else:
                print "Couldn't resize %s" % (icon)


    def do_category(self, icons, cat):
        for a in cat['achievements']:
            icons.add(a['icon'])

        for c in cat.get('categories', []):
            self.do_category(icons, c)
