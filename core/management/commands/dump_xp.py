from django.core.management.base import BaseCommand, CommandError

from thing.models import Character

# ---------------------------------------------------------------------------

MAX_LEVEL = 100
XP_PER_LEVEL = [
    0, 400, 900, 1400, 2100, 2800, 3600, 4500, 5400, 6500, # 1-10
    6700, 7000, 7700, 8700, 9700, 10800, 11900, 13100, 14200, 15400, # 11-20
    16600, 17900, 19200, 20400, 21800, 23100, 24400, 25800, 27100, 29000, # 21-30
    31000, 33300, 35700, 38400, 41100, 44000, 47000, 49900, 53000, 56200, # 31-40
    74300, 78500, 82800, 87100, 91600, 96300, 101000, 105800, 110700, 115700, # 41-50
    120900, 126100, 131500, 137000, 142500, 148200, 154000, 159900, 165800, 172000, # 51-60
    254000, 275000, 301000, 328000, 359000, 367000, 374000, 381000, 388000, 395000, # 61-70
    405000, 415000, 422000, 427000, 432000, 438000, 445000, 455000, 462000, 474000, # 71-80
    482000, 487000, 492000, 497000, 506000, 517000, 545000, 550000, 556000, 562000, # 81-90
    774800, 783900, 790400, 798200, 807300, 815100, 821600, 830700, 838500, 846300, # 91-100
]

# ---------------------------------------------------------------------------

class Command(BaseCommand):
    def handle(self, *args, **options):
        totalXP = 0

        for character in Character.objects.filter(bnetaccount__user__username=args[0]).order_by('-level').prefetch_related('realm'):
            print '%s-%s (%d + %d)' % (character.realm.name, character.name, character.level, character.current_xp)

            charXP = 0
            startLevel = 56 if character.cls_id == 6 else 1

            for i in range(startLevel, min(MAX_LEVEL, character.level) + 1):
                print 'Level %d: +%d' % (i, XP_PER_LEVEL[i - 1])
                charXP += XP_PER_LEVEL[i - 1]

            if character.level < MAX_LEVEL:
                print 'Current: %d' % (min(XP_PER_LEVEL[character.level], character.current_xp))
                charXP += min(XP_PER_LEVEL[character.level], character.current_xp)

            print 'TOTAL: %d' % (charXP)
            print '-----------'

            totalXP += charXP

        print totalXP

# ---------------------------------------------------------------------------
