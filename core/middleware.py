from django.utils import timezone

# ---------------------------------------------------------------------------

class ThingMiddleware(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if request.user.is_authenticated and not request.path.startswith('/api/upload'):
            request.user.profile.last_visit = timezone.now()
            request.user.profile.save(update_fields=['last_visit'])

        return response

# ---------------------------------------------------------------------------
