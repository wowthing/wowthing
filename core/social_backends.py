import hashlib

from social_core.backends.oauth import BaseOAuth2

class BattleNetUS(BaseOAuth2):
    name = 'bnet-us'
    AUTHORIZATION_URL = 'https://us.battle.net/oauth/authorize'
    ACCESS_TOKEN_URL = 'https://us.battle.net/oauth/token'
    DEFAULT_SCOPE = ['wow.profile']
    REDIRECT_STATE = False
    EXTRA_DATA = [
        ('refresh_token', 'refresh_token', True),
        ('expires_in', 'expires'),
        ('token_type', 'token_type', True)
    ]

    def get_user_details(self, response):
        """ Return user details from Battle.net account """
        stuff = '%s%s' % (response.get('id'), response.get('battletag'))
        return dict(
            username=hashlib.shake_128(stuff.encode('utf-8')).hexdigest(8),
        )

    def user_data(self, access_token, *args, **kwargs):
        """ Loads user data from service """
        return self.get_json(
            'https://us.battle.net/oauth/userinfo',
            params={'access_token': access_token}
        )

class BattleNetEU(BaseOAuth2):
    name = 'bnet-eu'
    ID_KEY = 'accountId'
    AUTHORIZATION_URL = 'https://eu.battle.net/oauth/authorize'
    ACCESS_TOKEN_URL = 'https://eu.battle.net/oauth/token'
    DEFAULT_SCOPE = ['wow.profile']
    REDIRECT_STATE = False
    EXTRA_DATA = [
        ('refresh_token', 'refresh_token', True),
        ('expires_in', 'expires'),
        ('token_type', 'token_type', True)
    ]

    def get_user_details(self, response):
        """ Return user details from Battle.net account """
        return dict(
            username=hashlib.shake_128(response.get('id') + response.get('battletag')).hexdigest(8),
        )

    def user_data(self, access_token, *args, **kwargs):
        """ Loads user data from service """
        return self.get_json(
            'https://eu.api.blizzard.com/wow/user/characters',
            params={'access_token': access_token}
        )
