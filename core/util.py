import datetime
import json
import locale
import pytz
import redis
import time

from django.conf import settings
from django.contrib.auth.models import User
from django.db import connection, transaction
from django.shortcuts import *

from core.models import Item

# ---------------------------------------------------------------------------

locale.setlocale(locale.LC_ALL, '')

# ---------------------------------------------------------------------------

def render_error(request, title, text):
    return render(
        request,
        'thing/error.html',
        dict(
            error_title=title,
            error_text=text,
        ),
    )

# ---------------------------------------------------------------------------

def json_dump(obj):
    return json.dumps(obj, separators=(',', ':'))

def json_response(text):
    return HttpResponse(text, content_type='application/json')

def json_response_encode(obj):
    return json_response(json_dump(obj))

# ---------------------------------------------------------------------------

def update_fields(object, data):
    updated = []
    for k, v in data.items():
        try:
            current = getattr(object, k)
        except AttributeError:
            pass
        else:
            if current != v:
                #print k, '->', repr(current), '!=', repr(v)
                setattr(object, k, v)
                updated.append(k)

    return updated

# ---------------------------------------------------------------------------

class TimerThing:
    def __init__(self, name):
        self.times = []
        self.add_time(name)

    def add_time(self, name):
        self.times.append([time.time(), name])

    def finished(self):
        m = 0
        for t, name in self.times:
            m = max(m, len(name))

        format_string = '%%-%ds  %%.3fs' % (m)
        sep = '-' * (m + 8)

        print(sep)
        print('TimerThing: %s' % (self.times[0][1]))
        print(sep)
        for i in range(1, len(self.times)):
            t, name = self.times[i]
            print(format_string % (name, t - self.times[i - 1][0]))
        print(sep)
        print(format_string % ('total', self.times[-1][0] - self.times[0][0]))
        print(sep)

# ---------------------------------------------------------------------------

def get_redis_client():
    host, port, db = settings.REDIS
    r = redis.StrictRedis(host=host, port=port, db=db)
    return r

# ---------------------------------------------------------------------------

def get_public_user(request, username=None, user_id=None):
    if username is not None and request.user.is_authenticated and request.user.username.lower() == username.lower():
        public = False
        user = request.user
    elif user_id is not None and request.user.is_authenticated and request.user.id == int(user_id):
        public = False
        user = request.user
    else:
        public = True
        if username:
            user = get_object_or_404(User, username__iexact=username, profile__public=True)
        else:
            user = get_object_or_404(User, pk=user_id, profile__public=True)

    # Redirect to the correct case
    if username and username != user.username:
        return redirect(request.path.replace(username, user.username)), None
    else:
        return public, user

# ---------------------------------------------------------------------------

def add_new_items(item_ids):
    # Add any new core.Item objects
    with transaction.atomic():
        # Lock the table :siren:
        cursor = connection.cursor()
        cursor.execute('LOCK TABLE core_item')

        # See if those items already exist
        existing = set(Item.objects.filter(pk__in=item_ids).values_list('pk', flat=True))

        # Create any new items
        new_items = []
        for itemID in item_ids:
            if itemID not in existing:
                new_items.append(Item(
                    id=itemID,
                    name='###UPDATE###',
                ))

        Item.objects.bulk_create(new_items)

# ---------------------------------------------------------------------------

def format_currency(copper):
    c = copper % 100
    s = int(float(copper) / 100) % 100
    g = int(float(copper) / 10000)
    parts = []

    if s == 0 and g == 0:
        parts.append('%dc' % c)
    else:
        parts.append('%02dc' % c)

        if g == 0:
            parts.append('%ds' % s)
        else:
            parts.append('%02ds' % s)
            parts.append('%sg' % locale.format('%d', g, grouping=True))

    parts.reverse()
    return ' '.join(parts)

# ---------------------------------------------------------------------------

def round_to_nearest(n, nearest):
    return int(nearest * round(float(n) / nearest))

# ---------------------------------------------------------------------------

def utctimestamp(t):
    return datetime.datetime.utcfromtimestamp(t).replace(tzinfo=pytz.utc)

# ---------------------------------------------------------------------------
