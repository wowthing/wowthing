from django.db import models

# ---------------------------------------------------------------------------

class GarrisonBuilding(models.Model):
    PLOT_CHOICES = (
        (12, 'Small'),
        (13, 'Medium'),
        (14, 'Large'),
        (28, 'Mine'),
        (29, 'Herb'),
        (31, 'Fish'),
        (42, 'Pet'),
    )

    id = models.SmallIntegerField(primary_key=True)
    alliance_name = models.CharField(max_length=32)
    horde_name = models.CharField(max_length=32)
    icon = models.CharField(max_length=64)
    level = models.SmallIntegerField()
    plot = models.SmallIntegerField(choices=PLOT_CHOICES)
    work_orders = models.SmallIntegerField()

# ---------------------------------------------------------------------------
