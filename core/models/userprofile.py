import hashlib
import random
import string

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.crypto import get_random_string

# ---------------------------------------------------------------------------

class UserProfile(models.Model):
    SORT_TYPES = (
        'default',
        #'realm',
        'name',
        '-name',
        'level',
        '-level',
        'itemlevel',
        '-itemlevel',
    )
    GROUP_TYPES = (
        'realm',
        'faction',
        'tag',
        'class',
    )

    user = models.OneToOneField(User, related_name='profile', on_delete=models.CASCADE)

    code = models.CharField(max_length=16, default='')
    api_key = models.CharField(max_length=64, default='')

    email_verified = models.BooleanField(default=False)
    email_verify_key = models.CharField(max_length=40, default='')
    raidgroups_enabled = models.BooleanField(default=False)

    last_visit = models.DateTimeField(default=timezone.now)

    public = models.BooleanField(default=True)
    anonymized = models.BooleanField(default=True)
    show_in_leaderboard = models.BooleanField(default=False)

    overview_show_realm = models.BooleanField(default=True)
    overview_show_race_icon = models.BooleanField(default=True)
    overview_show_class_icon = models.BooleanField(default=True)
    overview_show_rested = models.BooleanField(default=True)
    overview_show_played = models.BooleanField(default=True)
    overview_show_gold = models.BooleanField(default=True)
    overview_show_ail = models.BooleanField(default=True)
    overview_show_world_quests = models.BooleanField(default=True)
    overview_show_honor = models.BooleanField(default=True)
    overview_show_totals = models.BooleanField(default=True)
    
    overview_sort = models.CharField(max_length=128, default='default')
    overview_group = models.CharField(max_length=128, default='default')

    overview_show_currencies = ArrayField(models.IntegerField(), default=list)
    overview_show_lockouts = ArrayField(models.IntegerField(), default=list)

    gear_show_item_level = models.BooleanField(default=True)
    gear_show_bags = models.BooleanField(default=True)

    lockouts_ignore_old_expired_raids = models.BooleanField(default=True)
    lockouts_ignore_current_expired_raids = models.BooleanField(default=False)

    hide_characters = ArrayField(models.IntegerField(), default=list)
    hide_currencies = ArrayField(models.IntegerField(), default=list)

    def save(self, *args, **kwargs):
        if self.code == '':
            self.generate_code()

        if self.api_key == '':
            self.generate_api_key()

        super().save(*args, **kwargs)

    def generate_code(self):
        self.code = get_random_string(16)

    def generate_api_key(self):
        self.api_key = get_random_string(64)

    def as_dict(self):
        return dict(
            overview_show_realm=self.overview_show_realm,
            overview_show_race_icon=self.overview_show_race_icon,
            overview_show_class_icon=self.overview_show_class_icon,
            overview_show_rested=self.overview_show_rested,
            overview_show_played=self.overview_show_played,
            overview_show_gold=self.overview_show_gold,
            overview_show_ail=self.overview_show_ail,
            overview_show_currencies=self.overview_show_currencies,
            overview_show_lockouts=self.overview_show_lockouts,
            overview_show_world_quests=self.overview_show_world_quests,
            overview_show_honor=self.overview_show_honor,
            overview_show_totals=self.overview_show_totals,
            overview_sort=self.overview_sort,
            overview_group=self.overview_group,
            gear_show_item_level=self.gear_show_item_level,
            gear_show_bags=self.gear_show_bags,
            lockouts_ignore_old_expired_raids=self.lockouts_ignore_old_expired_raids,
            lockouts_ignore_current_expired_raids=self.lockouts_ignore_current_expired_raids,
            hide_characters=self.hide_characters,
            hide_currencies=self.hide_currencies,
        )

    def change_email(self, email):
        username = self.user.username
        if isinstance(username, unicode):
            username = username.encode('utf-8')

        salt = hashlib.sha1(str(random.random())).hexdigest()[:5]

        self.user.email = email
        self.user.save(update_fields=['email'])

        self.email_verified = False
        self.email_verify_key = hashlib.sha1('%s%s' % (salt, username)).hexdigest()
        self.save(update_fields=['email_verified', 'email_verify_key'])

        self.send_activation_email()

    def send_activation_email(self):
        ctx_dict = dict(
            activation_key=self.email_verify_key,
            expiration_days=settings.EMAIL_ACTIVATION_DAYS,
            user=self.user,
        )
        subject = 'WoWthing: please confirm your email address'
        message = render_to_string('rego/activation_email.txt', ctx_dict)

        self.user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)

    def verify_email(self, key):
        if key == self.email_verify_key:
            self.email_verified = True
            self.email_verify_key = ''
            self.save(update_fields=['email_verified', 'email_verify_key'])
            return True
        else:
            return False

# ---------------------------------------------------------------------------
# Magical hook so this gets called when a new user is created
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        UserProfile.objects.create(user=instance)

models.signals.post_save.connect(create_user_profile, sender=User)

# ---------------------------------------------------------------------------
