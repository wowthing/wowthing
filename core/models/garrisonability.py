from django.db import models

# ---------------------------------------------------------------------------

class GarrisonAbility(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    name = models.CharField(max_length=64, default='')
    effect = models.TextField(default='')
    icon = models.CharField(max_length=128, default='')
    quality = models.SmallIntegerField(default=0)

# ---------------------------------------------------------------------------
"""
{
    "id":4,
    "category":1,
    "name":"Orcslayer",
    "description":"Increases success chance when fighting orcs.",
    "icon":"achievement_boss_general_nazgrim",
    "flags":1,
    "alternatefactionability":0,
    "followers":[],
    "counters":{
        "11":{
            "ability":4,
            "type":11,
            "category":1,
            "name":"Orc",
            "description":"Lok'tar ogar!",
            "icon":"achievement_boss_general_nazgrim"
        }
    },
    "effects":[
        {
            "ability":4,
            "countermechanic":11,
            "type":0,
            "missionparty":0,
            "amount1":0,
            "amount2":100,
            "amount3":100,
            "amount4":0,
            "race":0,
            "hours":0
        }
    ]
}
"""
