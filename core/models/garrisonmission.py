from django.contrib.postgres.fields import ArrayField
from django.db import models

# ---------------------------------------------------------------------------

class GarrisonMission(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    base_bonus_chance = models.SmallIntegerField()
    cost = models.SmallIntegerField()
    description = models.TextField()
    experience = models.IntegerField()
    followers = models.SmallIntegerField()
    item_level = models.SmallIntegerField()
    level = models.SmallIntegerField()
    mechanic_type = models.SmallIntegerField()
    mission_time = models.IntegerField()
    mission_type = models.SmallIntegerField()
    name = models.CharField(max_length=64)
    rewards = models.TextField()

    abilities = models.ArrayField(models.IntegerField(), default=list)

# ---------------------------------------------------------------------------
