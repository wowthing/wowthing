from django.db import models

# ---------------------------------------------------------------------------

class GarrisonCounter(models.Model):
    id = models.SmallIntegerField(primary_key=True)
    name = models.CharField(max_length=32)
    description = models.TextField()
    icon = models.CharField(max_length=64)

# ---------------------------------------------------------------------------
