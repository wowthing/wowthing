from django.db import models

# ---------------------------------------------------------------------------

class CharacterSpecialization(models.Model):
    id = models.IntegerField(primary_key=True)
    cls = models.ForeignKey('core.CharacterClass', related_name='specializations', on_delete=models.CASCADE)
    name = models.CharField(max_length=16)
    icon = models.CharField(max_length=40)

    def as_dict(self):
        return dict(
            id=self.id,
            cls=self.cls_id,
            name=self.name,
            icon=self.icon,
        )

# ---------------------------------------------------------------------------
