from django.contrib.auth.models import User
from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import timezone

from . import Realm
#from thing.helpers import duration

# ---------------------------------------------------------------------------

class BNetAccount(models.Model):
    id = models.AutoField(primary_key=True)

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    account_id = models.IntegerField(default=0)
    region = models.CharField(max_length=2)
    token_json = models.TextField(default='')
    token_expires = models.DateTimeField(blank=True, null=True)
    battletag = models.CharField(max_length=32, default='')

    toys = ArrayField(models.IntegerField(), default=list)

    last_update = models.DateTimeField(blank=True, null=True)

    def last_updated(self):
        if self.last_update is None:
            return 'Never'
        else:
            return '%s ago' #% (duration((timezone.now() - self.last_update).total_seconds()))

    def realm_first_ids(self, hide=[]):
        realm_ids = [r for r in self.characters.values_list('realm', flat=True).distinct() if r not in hide]
        long_names = Realm.objects.filter(id__in=realm_ids).values_list('long_name').distinct()
        return Realm.objects.filter(long_name__in=long_names, first_realm=True).values_list('id', flat=True)

# ---------------------------------------------------------------------------
