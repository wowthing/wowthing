from django.db import models

# ---------------------------------------------------------------------------

class CharacterRace(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=32)
    side = models.CharField(max_length=8)
    female_icon = models.CharField(max_length=64, default='')
    male_icon = models.CharField(max_length=64, default='')

    def as_dict(self):
        return dict(
            id=self.id,
            name=self.name,
            side=self.side,
            male_icon=self.male_icon,
            female_icon=self.female_icon,
        )

# ---------------------------------------------------------------------------
