const child = require('child_process'),
    gulp = require('gulp'),
    batch = require('gulp-batch'),
    clean_css = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    less = require('gulp-less'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    touch = require('gulp-touch-fd'),
    uglify = require('gulp-uglify'),
    watch = require('gulp-watch');

function compile_handlebars(cb) {
    return child.execFile('./compile_templates.sh');
}

function css_less() {
    return gulp.src('assets/less/theme-default/bootstrap.less')
        .pipe(less())
        .pipe(clean_css())
        .pipe(rename('wowthing.min.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(touch());
}

function css_sass() {
    return gulp.src('assets/scss/theme.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(clean_css())
        .pipe(rename('theme.min.css'))
        .pipe(gulp.dest('dist/css'))
        .pipe(touch());
}

function js_achievements() {
    return build_js('achievements', ['assets/generated/achievements.js']);
}

function js_highcharts() {
    return build_js('highcharts', ['assets/js/oldlibs/highcharts-5.0.7.min.js']);
}

function js_libs() {
    return build_js('libs', [
        'assets/js/oldlibs/jquery-3.3.1.min.js',
        'assets/js/oldlibs/html.sortable-0.1.6.js',
        'assets/js/oldlibs/moment-2.22.2.min.js',
        'assets/js/oldlibs/handlebars.runtime-v2.0.0.js',
        'assets/js/lib/popper-1.15.0.min.js',
        'assets/js/lib/tippy-4.3.4.min.js',
        'assets/js/lib/bootstrap-4.3.1.min.js',
        'assets/js/lib/bootstrap3-typeahead.min.js',
    ]);
}

function js_oldlibs() {
    return build_js('oldlibs', [
        'assets/js/oldlibs/jquery-3.3.1.min.js',
        'assets/js/oldlibs/html.sortable-0.1.6.js',
        'assets/js/oldlibs/moment-2.22.2.min.js',
        'assets/js/oldlibs/handlebars.runtime-v2.0.0.js',
        'assets/js/oldlibs/bootstrap-3.4.1.min.js',
        'assets/js/oldlibs/bootstrap-editable-1.5.1.min.js',
        'assets/js/oldlibs/bootstrap3-typeahead.js',
        'assets/js/lib/popper-1.15.0.min.js',
        'assets/js/lib/tippy-4.3.4.min.js',
    ]);
}

function js_transmog() {
    return build_js('transmog', ['assets/generated/transmog.js']);
}

function js_wowthing() {
    return build_js('wowthing', [
        'assets/js/wowthing/*.js',
        'assets/generated/compiled_templates.js',
        'assets/generated/data.js',
    ]);
}

function build_js(name, sources) {
    return gulp.src(sources)
        .pipe(concat(name))
        .pipe(uglify({
            compress: {
            },
            mangle: {
                reserved: ['window', 'Popper', 'tippy'],
            },
            output: {
                //beautify: true,
            },
            parse: {
            },
        }))
        .pipe(rename(name + '.min.js'))
        .pipe(gulp.dest('dist/js'));
}

gulp.task('default',  gulp.series(
    compile_handlebars,
    gulp.parallel(
        css_less,
        css_sass,
        js_achievements,
        js_highcharts,
        js_libs,
        //js_oldlibs,
        js_transmog,
        js_wowthing)
));

gulp.task('sass', function () {
    gulp.watch('./assets/scss/**/*.scss', { ignoreInitial: false, usePolling: true }, css_sass);
});
