from django.conf.urls import include, url

from . import views

app_name = 'intapi'
urlpatterns = [
    url(r'^auctions/(?P<cat>mounts|pets|toys)/(?P<realms>all|mine)/$', views.auctions),
    url(r'^auctions/extra_pets/$', views.extra_pets),
    url(r'^dynamic_data/$', views.dynamic_data, name='dynamic_data'),
    url(r'^goldhistory/$', views.gold_history),
    url(r'^goldhistory/(?P<interval>[hdm])/(?P<period>[0-9a-z]+)/$', views.gold_history),
    url(r'^item_image/(?P<item_id>\d+)/(?P<size>\d+)/$', views.item_image),
    url(r'^leaderboards/$', views.leaderboards),
    #url(r'^userdata/(?P<user_id>\d+)/$', views.userdata),
    url(r'^userdata/(?P<code>[0-9A-Za-z]{16})/$', views.userdata_code, name='userdata_code'),
    url(r'^useritems/$', views.useritems),
]
