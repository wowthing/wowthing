#!/bin/sh

python manage.py migrate || exit 1
python manage.py build_js || exit 1
node_modules/.bin/gulp || exit 1
python manage.py collectstatic --noinput || exit 1

exec .local/bin/gunicorn wowthing.wsgi:application \
    --name wowthing \
    --bind 0:8000 \
    --log-level info
