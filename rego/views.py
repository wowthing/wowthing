from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import *
from django.template import RequestContext
from django.urls import reverse_lazy

from registration import signals
from registration.views import RegistrationView

from core.models import UserProfile
from rego.forms import RecaptchaRegistrationForm

# ---------------------------------------------------------------------------

class RecaptchaRegistrationView(RegistrationView):
    form_class = RecaptchaRegistrationForm
    success_url = reverse_lazy('home_redirect')

    def register(self, request, **cleaned_data):
        # Try creating the new user
        username, email, password = cleaned_data['username'], cleaned_data['email'], cleaned_data['password1']
        User.objects.create_user(username, email, password)

        # Log in as the new user
        new_user = authenticate(username=username, password=password)
        login(request, new_user)

        # Send the activation email
        new_user.profile.change_email(email)

        signals.user_registered.send(sender=self.__class__, user=new_user, request=request)

        return new_user

# ---------------------------------------------------------------------------

def activate_email(request, key):
    profile = get_object_or_404(UserProfile, email_verify_key=key)
    if profile.verify_email(key):
        if request.user.is_authenticated():
            messages.success(request, 'Your e-mail address is now verified.')
            return redirect(reverse_lazy('home_redirect'))
        else:
            return redirect(reverse_lazy('registration_activate_success'))

    else:
        return render_to_response(
            'rego/activation_failed.html',
            {},
            RequestContext(request),
        )

# ---------------------------------------------------------------------------

def activate_success(request):
    return render_to_response(
        'rego/activation_success.html',
        {},
        RequestContext(request),
    )

# ---------------------------------------------------------------------------
