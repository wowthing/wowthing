# ---------------------------------------------------------------------------
# Follower overrides
followers_override = {
    498: [498, 'Altruis the Sufferer / Kayn Sunfury'],
    595: [498, 'Altruis the Sufferer / Kayn Sunfury'],
    718: [718, 'Akama / Shade of Akama'],
    719: [718, 'Akama / Shade of Akama'],
}

# ---------------------------------------------------------------------------
# Followers available
followers_available = {
    # All
    0: [
        986, # Meatball
    ],
    # Warrior
    1: [
        714, # Dvalen Ironrune
        989, # Eitrigg / Lord Darius Crowley
        709, # Finna Bjornsdottir
        715, # Hodir
        711, # Hymdall
        712, # King Ymiron
        708, # Ragnvald Drakeborn
        710, # Svergan Stormcloak
        713, # Thorim
    ],
    # Paladin
    2: [
        757, # Aponi Brightmane
        758, # Arator the Redeemer
        756, # Delas Moonfang
        755, # Justicar Julia Celeste
        478, # Lady Liadrin
        759, # Lothraxion
        480, # Lord Maxwell Tyrosus
        1001,# Maximillian of Northshire
        1000,# Nerus Moonfang
        479, # Vindicator Boros
    ],
    # Hunter
    3: [
        746, # Addie Fizzlebog
        744, # Beastmaster Hilaire
        593, # Emmarel Shadewarden
        748, # Halduron Brightwing
        745, # Hemet Nesingwary
        747, # Huntsman Blake
        742, # Loren Stormhoof
        996, # Nighthuntress Syrenne
        743, # Rexxar
    ],
    # Rogue
    4: [
        780, # Fleet Admiral Tethys
        778, # Garona Halforcen
        988, # Lilian Voss / Princess Tess Greymane
        779, # Lord Jorach Ravenholdt
        890, # Marin Noggenfogger
        893, # Master Mathias Shaw
        892, # Taoshi
        891, # Valeera Sanguinar
        591, # Vanessa VanCleef
    ],
    # Priest
    5: [
        1002,# Aelthalyste
        875, # Alonsus Faol
        856, # Calia Menethil
        857, # High Priestess Ishanah
        873, # Mariella Ward
        874, # Natalie Seline
        872, # Sol
        871, # Yalia Sagewhisper
        870, # Zabra Hexx
    ],
    # Death Knight
    6: [
        854, # Amal'thazad
        839, # High Inquisitor Whitemane
        855, # Highlord Darion Mograine
        599, # Koltira Deathweaver
        1003,# Minerva Ravensorrow
        586, # Nazgrim
        853, # Rottgut
        584, # Thassarian
        838, # Thoras Trollbane
    ],
    # Shaman
    7: [
        615, # Avalanchion the Unbroken
        613, # Baron Scaldius
        610, # Consular Celestos
        609, # Duke Hydraxis
        611, # Farseer Nobundo
        992, # Magatha Grimtotem
        614, # Muln Earthfury
        612, # Rehgar Earthfury
        608, # Stormcaller Mylra
    ],
    # Mage
    8: [
        994, # Aethas Sunreaver
        724, # Arcane Destroyer
        716, # Archmage Kalec
        717, # Archmage Modera
        762, # Archmage Vargoth
        726, # Esara Verrinde
        761, # Meryl Felstorm
        723, # Millhouse Manastorm
        725, # Ravandwyr
        995, # The Great Akazamzarak
    ],
    # Warlock
    9: [
        616, # Calydus
        621, # Eredar Twins
        619, # Jubeka Shadowbreaker
        997, # Kanrethad Ebonlocke
        617, # Kira Iresoul
        590, # Lulubelle Fizzlebang
        589, # Ritssyn Flamescowl
        620, # Shinfel Blightsworn
        618, # Zinnin Smythe
    ],
    # Monk
    10: [
        605, # Angus Ironfist
        607, # Aegira
        998, # Brewer Almai
        596, # Chen Stormstout
        606, # Hiro
        588, # Li Li Stormstout
        604, # Sylara Steelsong
        603, # Taran Zhu
        602, # The Monkey King
    ],
    # Druid
    11: [
        643, # Archdruid Hamuul Runetotem
        645, # Brightwing
        641, # Broll Bearmantle
        646, # Keeper Remulos
        644, # Mylune
        639, # Naralex
        642, # Sylendra Gladesong
        999, # Thisalee Crow
        640, # Zen'tabra
    ],
    # Demon Hunter
    12: [
        718, # Akama/Shade of Akama
        499, # Allari the Souleater
        722, # Asha Ravensong
        594, # Belath Dawnblade
        807, # Jace Darkweaver
        498, # Altruis the Sufferer/Kayn Sunfury
        721, # Kor'vas Bloodthorn
        990, # Lady S'theno
        720, # Matron Mother Malevolence
    ],
}

# ---------------------------------------------------------------------------
# Follower optimization
followers_optimization = {
    'x3noncs@reddit': {
        # Item sets
        'sets': {
            'hazard': [
                968, # Memento of the Lightforged [+30% success chance]
                969, # Writ of Holy Orders [-30% duration]
                995, # Xenic Tincture [+50% success vs Hazards]
            ],
            'minion': [
                968, # Memento of the Lightforged [+30% success chance]
                969, # Writ of Holy Orders [-30% duration]
                970, # Lightburst Charge [+50% sucess vs Minions]
            ], # Memento of the Lightforged, Lightburst Charge, Writ of Holy Orders
            'spell': [
                968, # Memento of the Lightforged [+30% success chance]
                969, # Writ of Holy Orders [-30% duration]
                996, # Runewarded Lightblade [+50% success vs Spells]
            ],
            'fast': [
                969, # Writ of Holy Orders [-30% duration]
                964, # Satchel of Lucidity [-25% duration]
                966, # Sanctified Armaments of the Light [+50% success if <8 hours]
            ],
            'slow': [
                968, # Memento of the Lightforged [+30% success chance]
                967, # Exalted Xenedar Hammer [+50% success if >8 hours]
                [1000, 759], # Azurelight Sapphire/Vial of Timeless Breath [+40% success if >8 hours]
            ],
        },
        # Warrior
        1: {
            714: 'spell' , # Dvalen Ironrune
            989: 'hazard', # Eitrigg/Lord Darius Crowley
            709: 'spell' , # Finna Bjornsdottir
            715: 'minion', # Hodir
            708: 'minion', # Ragnvald Drakeborn
            713: 'hazard', # Thorim
            986: 'fast'  , # Meatball
        },
        # Paladin
        2: {
            758: 'minion', # Arator the Redeemer
            756: 'minion', # Delas Moonfang
            755: 'fast'  , # Justicar Julia Celeste
            480: 'hazard', # Lord Maxwell Tyrosus
            1001:'spell' , # Maximillian of Northshire
            1000:'slow'  , # Nerus Moonfang
            986: 'fast'  , # Meatball
        },
        # Hunter
        3: {
            746: 'fast'  , # Addie Fizzlebog
            593: 'hazard', # Emmarel Shadewarden
            748: 'hazard', # Halduron Brightwing
            745: 'minion', # Hemet Nesingwary
            747: 'spell' , # Huntsman Blake
            743: 'minion', # Rexxar
            986: 'fast'  , # Meatball
        },
        # Rogue
        4: {
            780: 'hazard', # Fleet Admiral Tethys
            778: 'hazard', # Garona Halforcen
            988: 'spell' , # Lilian Voss / Princess Tess Greymane
            779: 'minion', # Lord Jorach Ravenholdt
            893: 'minion', # Master Mathias Shaw
            591: 'spell' , # Vanessa VanCleef
            986: 'fast'  , # Meatball
        },
        # Priest
        5: {
            1002:'minion', # Aelthalyste
            875: 'hazard', # Alonsus Faol
            856: 'minion', # Calia Menethil
            857: 'hazard', # High Priestess Ishanah
            871: 'spell' , # Yalia Sagewhisper
            870: 'spell' , # Zabra Hexx
            986: 'fast'  , # Meatball
        },
        # Death Knight
        6: {
            855: 'spell' , # Highlord Darion Mograine
            599: 'fast'  , # Koltira Deathweaver
            1003:'hazard', # Minerva Ravensorrow
            586: 'hazard', # Nazgrim
            584: 'spell' , # Thassarian
            838: 'minion', # Thoras Trollbane
            986: 'fast'  , # Meatball
        },
        # Shaman
        7: {
            615: 'spell' , # Avalanchion the Unbroken
            610: 'minion', # Consular Celestos
            609: 'slow'  , # Duke Hydraxis
            611: 'hazard', # Farseer Nobundo
            614: 'hazard', # Muln Earthfury
            608: 'fast'  , # Stormcaller Mylra
            986: 'fast'  , # Meatball
        },
        # Mage
        8: {
            724: 'slow'  , # Arcane Destroyer
            762: 'minion', # Archmage Vargoth
            726: 'fast'  , # Esara Verrinde
            761: 'minion', # Meryl Felstorm
            725: 'hazard', # Ravandwyr
            995: 'spell' , # The Great Akazamzarak
            986: 'fast'  , # Meatball
        },
        # Warlock
        9: {
            616: 'hazard', # Calydus
            621: 'minion', # Eredar Twins
            619: 'fast'  , # Jubeka Shadowbreaker
            617: 'spell' , # Kira Iresoul
            620: 'slow'  , # Shinfel Blightsworn
            618: 'spell' , # Zinnin Smythe
            986: 'fast'  , # Meatball
        },
        # Monk
        10: {
            605: 'hazard', # Angus Ironfist
            998: 'minion', # Brewer Almai
            596: 'spell' , # Chen Stormstout
            606: 'fast'  , # Hiro
            604: 'hazard', # Sylara Steelsong
            602: 'minion', # The Monkey King
            986: 'fast'  , # Meatball
        },
        # Druid
        11: {
            643: 'minion', # Archdruid Hamuul Runetotem
            645: 'minion', # Brightwing
            641: 'hazard', # Broll Bearmantle
            646: 'spell' , # Keeper Remulos
            644: 'hazard', # Mylune
            642: 'spell' , # Sylendra Gladesong
            986: 'fast'  , # Meatball
        },
        # Demon Hunter
        12: {
            718: 'spell' , # Akama/Shade of Akama
            594: 'spell' , # Belath Dawnblade
            807: 'hazard', # Jace Darkweaver
            498: 'hazard', # Altruis the Sufferer/Kayn Sunfury
            721: 'minion', # Kor'vas Bloodthorn
            990: 'minion', # Lady S'theno
            986: 'fast'  , # Meatball
        },
    },
}
