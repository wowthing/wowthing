FROM nikolaik/python-nodejs:python3.7-nodejs10

ENV DJANGO_SETTINGS_MODULE settings.production

RUN useradd --create-home wowthing
USER wowthing
WORKDIR /home/wowthing

COPY --chown=wowthing:wowthing requirements/ requirements/
RUN pip install --user --no-warn-script-location -r requirements/production.txt

COPY --chown=wowthing:wowthing package*.json ./
RUN npm install

COPY --chown=wowthing:wowthing . .

ENTRYPOINT ["./run.sh"]
