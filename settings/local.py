import os

from .base import *

DEBUG = True
#DEBUG = False
#TEMPLATE_DEBUG = DEBUG

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'wowthing',                      # Or path to database file if using sqlite3.
        # The following settings are not used with sqlite3:
        'USER': 'freddie',
        'PASSWORD': 'Lo0pY',
        'HOST': 'datahaven',                      # Empty for localhost through domain sockets or '127.0.0.1' for localhost through TCP.
        'PORT': '',                      # Set to empty string for default.
    }
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': 'datahaven:11211',
        'KEY_PREFIX': 'wowthing',
    }
}

MEDIA_ROOT = '/tmp/wt-media/'
MEDIA_URL = '/media/'

#INSTALLED_APPS += ('debug_toolbar',)
#MIDDLEWARE_CLASSES = ('debug_toolbar.middleware.DebugToolbarMiddleware',) + MIDDLEWARE_CLASSES

INTERNAL_IPS = [
    '127.0.0.1',
]
for i in range(1, 255):
    INTERNAL_IPS.append('192.168.0.%d' % (i))

ALLOWED_HOSTS = ['*']

STATIC_ROOT = '/tmp/static'
STATICFILES_DIRS.append(os.path.join(BASE_DIR, 'assets'))

REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] = (
    'rest_framework.renderers.JSONRenderer',
    'rest_framework.renderers.BrowsableAPIRenderer',
)

BNET_REDIRECT_BASE = 'http://devthing.lan:8001%s'

REDIS = ('datahaven', 6379, 0)
