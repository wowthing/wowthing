# Generated by Django 2.1 on 2018-08-22 15:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auctions', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auction',
            name='time_left',
            field=models.SmallIntegerField(choices=[(0, 'Short'), (1, 'Medium'), (2, 'Long'), (3, 'Very Long')], default=0),
        ),
    ]
