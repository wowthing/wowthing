var WoWthing = WoWthing || {};
WoWthing.home = (function() {
    var loading = true,
        loaded_gold_history = false,
        loaded_items = 0,
        loaded_transmog = 0,
        loadHash,
        contentShown = true,
        settingsShown = false,
        currencySearch = [],
        chartStatus = JSON.parse(localStorage.getItem('chartStatus') || '{}'),
        gold_history = [],
        gold_history_interval = localStorage.getItem('gold_history_interval') || 'd',
        gold_history_period = localStorage.getItem('gold_history_period') || 'all',
        gold_min = {},
        lockoutSearch = [],
        realmList = [],
        realmListShow = [],
        recentAchievements = [],
        selectedCharacter,
        overviewGrouping = [],
        sortOrder = [],
        userdata,
        userItems = {},
        userQuests = {},
        userReputations,
        shownAchievementCategory = null,
        shownProgressCategory = null,
        shownReputationCategory = null,
        achievementCategoryMap = {},
        achievementMap = {},
        achievementSettings = JSON.parse(localStorage.getItem('achievementSettings') || '{"earned":true,"not_earned":true,"alliance":true,"horde":true}'),
        set_settings = JSON.parse(localStorage.getItem('set_settings') || '{}'),
        successInactive = JSON.parse(localStorage.getItem('successInactive') || 'false'),
        successEvent,
        successCharacter,
        characterMap = {},
        bnet_blurb = " Without an active token, WoWthing can't see any character list changes: created, deleted, renamed, transferred. Please visit <a href=\"/oauth/\">Link Battle.net Account</a> and click 'Add Battle.net account' to refresh the token.",
        templates = {
            overview: null,
            achievements: null,
            achievements_achievement: null,
            achievements_summary: null,
            currencies: null,
            gear: null,
            goldhistory: null,
            items: null,
            lockouts: null,
            progress: null,
            progress_section: null,
            reputations: null,
            reputations_section: null,
            sets: null,
            sets_inner: null,
            settings_hide: null,
            settings_currencies: null,
            settings_lockouts: null,
            transmog: null,
            transmog_inner: null,
        },
        cache = {
            home_content: null,
            home_loading: null,
            home_settings: null,
            home_settings_hide: null,
        };

    var onload = function() {
        // Cache objects
        for (var k in cache) {
            cache[k] = $('#' + k.replace(/_/g, '-'));
        }

        // Load templates
        for (var k in templates) {
            templates[k] = Handlebars.getTemplate('home_' + k);
        }

        selectedCharacter = localStorage.getItem('selectedCharacter:' + WoWthing.home._user_id);

        bind_events();
        register_helpers();
        load_data();
    };

    var bind_events = function() {
        $('.js-sidebar').on('click', event_sidebar_click);

        cache.home_settings_hide.on('click', '.js-toggle-all', function() {
            $('input', $(this).closest('.list-group')).prop('checked', this.checked);
        });
        cache.home_settings_hide.on('click', '.js-toggle', function() {
            var all_checked = true,
                $group = $(this).closest('.list-group'),
                $inputs = $('input.js-toggle', $group);
            for (var i = 0; i < $inputs.length; i++) {
                if (!$inputs[i].checked) {
                    all_checked = false;
                    break;
                }
            }

            $('.settings-realm input', $group).prop('checked', all_checked);
        });
    };

    var register_helpers = function() {
        // Overview
        Handlebars.registerHelper('totalCurrencies', function() {
            var html = '';
            for (var i = 0; i < userdata.profile.overview_show_currencies.length; i++) {
                var currencyID = userdata.profile.overview_show_currencies[i],
                    total = 0;
                
                for (var j = 0; j < userdata.characters.length; j++) {
                    var character = userdata.characters[j],
                        curr = character.currencies[currencyID];
                    total += (curr ? curr.have_value : 0);
                }

                html += '<td class="character-currency">' + WoWthing.util.commas(total) + '</td>';
            }
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('realmCurrencies', function() {
            var html = '';
            for (var i = 0; i < userdata.profile.overview_show_currencies.length; i++) {
                var currencyID = userdata.profile.overview_show_currencies[i];
                html += '<td class="character-currency" rel="tooltip" title="' + WoWthing.data.currencies[currencyID].name + '">';
                //html += WoWthing.util.get_icon_img(WoWthing.data.currencies[currencyID].icon, 16);
                html += WoWthing.util.get_openwow_icon(WoWthing.data.currencies[currencyID].icon, 16);
                html += '</td>';
            }
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('realmLockouts', function() {
            var html = '';
            for (var i = 0; i < userdata.profile.overview_show_lockouts.length; i++) {
                var instance = WoWthing.data.instances[userdata.profile.overview_show_lockouts[i]];
                html += '<td class="character-lockout">' + instance.short_name + '</td>';
            }
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('characterName', function() {
            var extra = (userdata.profile.overview_show_race_icon ? 1 : 0) + (userdata.profile.overview_show_class_icon ? 1 : 0),
                html = '<td class="character-name-' + extra + '">';

            if (userdata.profile.overview_show_race_icon) {
                html += WoWthing.util.character_race_icon(this);
            }
            if (userdata.profile.overview_show_class_icon) {
                html += WoWthing.util.character_class_icon(this);
            }

            if (WoWthing.home._public && WoWthing.home._anonymized) {
                html += this.name;
            }
            else {
                html += WoWthing.util.character_armory_link(this, this.region);

                if (userdata._tag_count > 0) {
                    var tag_set = false;
                    if (this.tag_id !== undefined) {
                        var tag = userdata.tags[this.tag_id];
                        if (tag !== undefined) {
                            tag_set = true;
                            html += '<a href="#" class="character-tag" data-pk="' + this.id + '" data-value="' + this.tag_id + '" data-title="Select tag"';
                            html += ' style="color:#' + tag.foreground + ';background:#' + tag.background + ';">' + tag.short + '</a>';
                        }
                    }
                    if (!tag_set) {
                        html += '<a href="#" class="character-tag" data-pk="' + this.id + '" data-value="0" data-title="Select tag">?</a>';
                    }
                }
            }

            html += '</td>';
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('characterLevel', function() {
            var level = this.level;
            if (level < WoWthing.data.max_player_level) {
                if (this.current_xp && this.level_xp) {
                    var partial = this.current_xp / this.level_xp;
                    if (partial > 0 && partial < 1) {
                        level = (level + partial);
                    }
                }
                level = (Math.floor(level * 10) / 10).toFixed(1);
            }
            return level;
        });
        Handlebars.registerHelper('characterRested', function() {
            if (this.rested_xp > 0) {
                var updated = moment(this.last_char_update),
                    secs = moment().diff(updated, 'seconds'),
                    bubble = this.level_xp / 20,
                    hours = (this.resting ? 8 : 32),
                    rested = this.rested_xp + (bubble * (secs / hours / 60 / 60)),
                    per = Math.min(150, rested / this.level_xp * 100);
                if (!isNaN(per)) {
                    return '+' + Math.floor(per) + '%';
                }
            }
        });
        Handlebars.registerHelper('characterProfession', function(profession) {
            var popover = '';
            if (profession) {
                var parentID = profession[0].profession.parent || profession[0].id,
                    order = WoWthing.data.grouped_professions[parentID],
                    title = WoWthing.data.professions[order[order.length - 1]].alliance_name,
                    byID = {};

                for (var k in profession) {
                    byID[profession[k].id] = profession[k];
                }

                popover += '<table class=\'profession-popover\'>';

                for (var i = 0; i < order.length; i++) {
                    // Blizzard data is weird, there are 2x "Enchanting" but the first one (higher ID) isn't actually used
                    if (i === (order.length - 2)) {
                        continue;
                    }

                    var professionID = order[i],
                        currentProf = byID[professionID];

                    popover += '<tr>';
                    popover += '<td>' + WoWthing.data.professions[professionID][this.side + '_name'] + '</td>';
                    if (currentProf !== undefined) {
                        popover += '<td' + (currentProf.current_skill >= currentProf.max_skill ? " class='highlight'" : '') + '>';
                        popover += '<span>' + currentProf.current_skill + '</span>/<span>' + currentProf.max_skill + '</span>';
                    }
                    else {
                        popover += "<td class='warning'><span>---</span>/<span>---</span>";
                    }
                    popover += '</td>';
                    popover += '</tr>';
                }

                popover += '</table>';
            }

            var html = '<td class="character-profession"' + WoWthing.util.fancy_tooltip(popover) + '>';
            if (profession) {
                var parentID = profession[0].profession.parent || profession[0].id,
                    index = WoWthing.data.grouped_professions[parentID].indexOf(profession[0].id);

                html += WoWthing.util.get_openwow_icon(profession[0].profession.icon, 16);

                if (index === 0 && profession[0].is_maxed) {
                    html += '<span class="highlight">' + profession[0].current_skill + '</span>';
                }
                else if (index > 0) {
                    html += '<span class="warning">' + profession[0].current_skill + '</span>';
                }
                else {
                    html += '<span>' + profession[0].current_skill + '</span>';
                }
            }
            html += '</td>';

            return new Handlebars.SafeString(html);
        });
        // World quests
        Handlebars.registerHelper('realmWorldQuests', function() {
            var html = '',
                wq_data = userdata.world_quests[this.region];

            for (var i = 0; i < 3; i++) {
                if (wq_data !== undefined && wq_data.expires[i] !== undefined) {
                    var faction_id = wq_data.factions[i],
                        horde_faction = WoWthing.data.factions[faction_id],
                        horde_icon = WoWthing.data.faction_icons[faction_id],
                        alliance_id = WoWthing.data.emissary_alliance[faction_id],
                        alliance_faction = WoWthing.data.factions[alliance_id],
                        alliance_icon = WoWthing.data.faction_icons[alliance_id],
                        tooltip = (horde_faction !== undefined ? horde_faction.name : 'Unknown faction');

                    // Split icons, hoo boy
                    if (alliance_faction !== undefined) {
                        html += '<td class="character-wq" rel="tooltip" title="' + alliance_faction.name + ' / ' + tooltip + '">';
                        html += '<div class="icon-split icon-split-16">';
                        html += WoWthing.util.get_openwow_icon(alliance_icon, 16, 'faction0');
                        html += WoWthing.util.get_openwow_icon(horde_icon, 16, 'faction1');
                        html += '</div>';
                        html += '</td>';
                    }
                    else {
                        html += '<td class="character-wq" rel="tooltip" title="' + tooltip + '">' + WoWthing.util.get_openwow_icon(horde_icon, 16) + '</td>';
                    }
                }
                else {
                    html += '<td class="character-wq" rel="tooltip" title="Unknown faction">' + WoWthing.util.get_openwow_icon(undefined, 16) + '</td>';
                }
            }

            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('characterWorldQuests', function() {
            var html = '',
                wq_data = userdata.world_quests[this.region];

            for (var i = 0; i < 3; i++) {
                if (this.level === WoWthing.data.max_player_level) {
                    var status = '<b>?</b>',
                        tooltip = 'Unknown faction';

                    if (wq_data !== undefined && wq_data.expires[i] !== undefined) {
                        var expires = wq_data.expires[i],
                            expires_moment = moment(expires).format('YYYY-MM-DD h:mm a'),
                            faction_id = wq_data.factions[i];

                        if (this.side === 'alliance') {
                            faction_id = WoWthing.data.emissary_alliance[faction_id] || faction_id;
                        }
                        
                        var faction = WoWthing.data.factions[faction_id],
                            faction_icon = WoWthing.data.faction_icons[faction_id];

                        if (faction !== undefined) {
                            tooltip = faction.name;

                            // See if we can find a matching thing in the character data
                            var found = false;
                            for (var j = 0; j < this.world_quests.length; j++) {
                                var wq = this.world_quests[j];
                                if (wq.expires === expires) {
                                    if (wq.finished) {
                                        status = '<span class="icon-ok"></span>';
                                        tooltip += '<br>Completed!';
                                    }
                                    else {
                                        status = '<span class="icon-' + (wq.completed > 0 ? 'star-half' : 'cancel') + '"></span>';
                                        tooltip += '<br><span class=\'exciting\'>Expires: ' + expires_moment + '</span>';
                                        tooltip += '<br>' + wq.completed + ' / ' + wq.required + ' quests completed';
                                    }
                                    found = true;
                                    break;
                                }
                            }

                            if (!found) {
                                status = '<span class="icon-cancel"></span>';
                                tooltip += '<br>Expires: ' + expires_moment;
                            }
                        }
                    }

                    html += '<td class="character-wq" data-tippy-content="' + tooltip + '">' + status + '</td>';
                }
                else {
                    html += '<td class="character-wq"></td>';
                }
            }

            return new Handlebars.SafeString(html);
        });
        
        // Azerite
        Handlebars.registerHelper('characterAzerite', function() {
            var html = '<td class="character-azerite"';
            if (this.azerite_level > 0) {
                var tooltip = "<span class='exciting'>Azerite Level " + this.azerite_level + '</span><br>';
                tooltip += WoWthing.util.commas(this.azerite_current_xp) + ' / ' + WoWthing.util.commas(this.azerite_level_xp) + ' XP';
                html += ' data-tippy-content="' + tooltip + '">';

                html += WoWthing.util.get_openwow_icon('inv_heartofazeroth', 16);
                html += (this.azerite_level + (this.azerite_current_xp / this.azerite_level_xp)).toFixed(1);
            }
            else {
                html += '>';
            }
            html += '</td>';

            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('characterEssences', function() {
            var html = '<td class="character-essences">',
                essences = this.essences || [];
            for (var i = 0; i < 3; i++) {
                var essenceId = essences[i];
                if (essenceId !== undefined) {
                    html += '<div></div>';
                }
                else {
                    html += '<div data-tippy-content="Empty essence slot"></div>';
                }
            }
            html += '</td>';
            return new Handlebars.SafeString(html);
        });
        
        // Currencies
        Handlebars.registerHelper('characterCurrencies', function() {
            var html = '';
            for (var i = 0; i < userdata.profile.overview_show_currencies.length; i++) {
                html += '<td class="character-currency"';

                if (this.currencies) {
                    var currencyID = userdata.profile.overview_show_currencies[i],
                        curr = this.currencies[currencyID],
                        value = (curr ? WoWthing.util.commas(curr.have_value) : 0),
                        highlights = WoWthing.data.currency_highlights[currencyID] || [],
                        found = false;

                    for (var j = 0; j < highlights.length; j++) {
                        var data = highlights[j];
                        if (curr && curr.have_value >= data[0]) {
                            html += 'data-tippy-content="' + data[2] + '"><span class="' + data[1] + '">' + value + '</span>';
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        var name = WoWthing.data.currencies[currencyID].name;
                        if (curr && curr.have_max > 0) {
                            html += 'data-tippy-content="' + value + ' / ' + WoWthing.util.commas(curr.have_max) + ' ' + name + '">';
                        }
                        else {
                            html += 'data-tippy-content="' + value + ' ' + name + '">';
                        }
                        html += value;
                    }
                }
                else {
                    html += '>';
                }

                html += '</td>';
            }
            return new Handlebars.SafeString(html);
        });

        // Lockouts
        Handlebars.registerHelper('characterLockouts', function() {
            var html = '',
                now = moment();

            for (var i = 0; i < userdata.profile.overview_show_lockouts.length; i++) {
                html += '<td class="character-lockout">';

                if (this.lockouts !== undefined) {
                    var instanceID = userdata.profile.overview_show_lockouts[i],
                        instance = WoWthing.data.instances[instanceID],
                        lockouts = this.lockouts[instanceID] || [],
                        expansion = WoWthing.data.expansions[instance.expansion];

                    if (this.level >= expansion.max_level) {
                        // FIXME: deal with multiple difficulties!
                        if (lockouts.length > 0) {
                            var lockout = lockouts[0],
                                expired = (lockout.expires === null || lockout.expires < now);
                            if (expired) {
                                html += '<span class="icon-cancel"></span>';
                            }
                            else {
                                var difficultyShort = WoWthing.data.difficulty_short[lockout.difficulty],
                                    instanceName = instance.name + (difficultyShort ? ' <a>[' + difficultyShort + ']</a>' : ''),
                                    incomplete = (lockout.killed < lockout.total);

                                html += '<span class="icon-star' + (incomplete ? '-half' : '') + '" title="' + instanceName;
                                if (lockout.total > 0) {
                                    html += ' (' + lockout.killed + '/' + lockout.total + ')';
                                }
                                else {
                                    html += ' (??/??)';
                                }
                                html += '" data-content="' + lockout_popover(lockout) + '" rel="popover"></span>';
                            }
                        }
                        else {
                            html += '<span class="icon-cancel"></span>';
                        }
                    }
                }
                html += '</td>';
            }
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('characterKeystone', function() {
            var html = '';
            if (this.level === WoWthing.data.max_player_level) {
                if (this.keystone_id > 0 && WoWthing.data.keystone_maps[this.keystone_id] !== undefined) {
                    var data = WoWthing.data.keystone_maps[this.keystone_id];
                    html += '<span rel="tooltip" title="' + data[1] + '">' + data[0] + '</span>&nbsp;' + this.keystone_level;
                }
                else {
                    html += '---';
                }
            }
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('characterKeystoneMax', function() {
            var html = '';
            if (this.level === WoWthing.data.max_player_level) {
                html += '<span class="' + (this.keystone_max > 0 ? 'highlight' : 'warning') + '"';
                html += ' rel="tooltip" title="Max level Mythic+ completed this week">' + this.keystone_max + '</span>';
            }
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('keystoneImage', function() {
            return new Handlebars.SafeString(WoWthing.util.get_openwow_icon('inv_relics_hourglass', 16));
        });

        Handlebars.registerHelper('allHiddenChecked', function() {
            var all = true;
            for (var i = 0; i < this.characters.length; i++) {
                if (!this.characters[i].hidden) {
                    all = false;
                    break;
                }
            }
            if (all) {
                return ' checked';
            }
        });
        Handlebars.registerHelper('hiddenChecked', function() {
            return (this.hidden ? ' checked' : '');
        });

        // Achievements
        Handlebars.registerHelper('achievementRecent', function() {
            return new Handlebars.SafeString(templates.achievements_achievement({
                cheev: this,
                earned: true,
            }));
        });
        Handlebars.registerHelper('achievementPoints', function() {
            var points = this.cheev.points,
                prev = WoWthing.data.achievement_prev[this.cheev.id];
            while (prev) {
                var prev_achievement = achievementMap[prev];
                points += prev_achievement.points;
                prev = WoWthing.data.achievement_prev[prev];
            }
            return points;
        });
        Handlebars.registerHelper('achievementSummaryBarStyle', function(name) {
            return ((name === 'Legacy' || name === 'Feats of Strength') ? '' : 'bg-success');
        });
        Handlebars.registerHelper('achievementCriteria', function() {
            // FIXME: just hide criteria for earned achievements
            if (this.earned) {
                return;
            }

            //console.log(this.cheev);

            var html = '',
                rep_faction_id = WoWthing.data.achievement_reps[this.cheev.id];
            if (this.cheev.criteria || rep_faction_id) {
                // Rep achievement hax
                if (rep_faction_id) {
                    var reps = userReputations[rep_faction_id];
                    if (reps) {
                        html += '<tr><td class="achievement-criteria" colspan="3">';

                        for (var i = 0; i < Math.min(3, reps.length); i++) {
                            var character = characterMap[reps[i][0]],
                                value = reps[i][1],
                                parts = format_criteria({type: 46}, value, 42000);

                            html += '<div class="progress"><div class="progress-bar progress-bar-success" style="width: ' + parts[0] + '%">';
                            html += '<span>' + parts[1] + '  (' + character.name + '-' + character.realm + ')</span></div></div>';
                        }

                        html += '</td></tr>';
                    }
                } // if rep_faction_id
                // Single criteria
                else if (this.cheev.criteria.length === 1) {
                    var criteriaID = this.cheev.criteria[0][0],
                        requiredQuantity = this.cheev.criteria[0][1],
                        criteria = WoWthing.data.achievement_criteria[criteriaID];
                    if (criteria === undefined) {
                        console.log(this.cheev.id, this.cheev.criteria);
                    }
                    var bar = (criteria.flags && (criteria.flags & 1 === 1)) || (criteria.type === 1);
                    console.log(criteria, bar);

                    // Single criteria progress bar, woo
                    if (bar) {
                        var crits = userdata.criteria[criteriaID];

                        // Account wide
                        if (this.cheev.accountWide) {
                            var value = 0;
                            if (crits) {
                                value = crits[0][1];
                            }

                            var parts = format_criteria(criteria, value, requiredQuantity);
                            var per = parts[0],
                                text = parts[1];

                            html += '<tr><td class="achievement-criteria" colspan="3">';
                            html += '<div class="progress"><div class="progress-bar progress-bar-success" style="width: ' + per + '%">';
                            html += '<span>' + text + '</span></div></div>';
                            html += '</td></tr>';
                        }
                        // Per character
                        else {
                            if (crits) {
                                var count = 0;
                                for (var i = 0; i < crits.length; i++) {
                                    var characterID = crits[i][0],
                                        value = crits[i][1],
                                        character = characterMap[characterID];

                                    if (value === 0) {
                                        continue;
                                    }

                                    if (character === undefined) {
                                        console.log("WARNING: undefined character with characterID " + characterID);
                                        continue;
                                    }

                                    // Check faction
                                    if (check_achievement_faction(character, this.cheev.faction)) {
                                        count++;
                                        if (count === 1) {
                                            html += '<tr><td class="achievement-criteria" colspan="3">';
                                        }

                                        var parts = format_criteria(criteria, value, requiredQuantity, character);
                                        var per = parts[0],
                                            text = parts[1];

                                        html += '<div class="progress"><div class="progress-bar progress-bar-success" style="width: ' + per + '%">';
                                        html += '<span>' + text + '  (' + character.name + '-' + character.realm + ')</span></div></div>';

                                        if (count === 3) {
                                            break;
                                        }
                                    }
                                }
                                if (count) {
                                    html += '</td></tr>';
                                }
                            }
                        }
                    }
                } // if single criteria
                // Multiple criteria
                else if (this.cheev.criteria.length > 1) {
                    // Account-wide
                    if (this.cheev.accountWide) {
                        html += '<tr><td class="achievement-criteria" colspan="3">';
                        for (var i = 0; i < this.cheev.criteria.length; i++) {
                            var criteriaID = this.cheev.criteria[i][0],
                                requiredQuantity = this.cheev.criteria[i][1],
                                criteria = WoWthing.data.achievement_criteria[criteriaID],
                                earned = check_criteria(criteriaID, criteria, requiredQuantity, undefined, this.cheev.faction),
                                desc = format_criteria(criteria)[1];

                            html += '<div class="achievement-criteria-box col-lg-4 col-md-6">';
                            html += '<span class="glyphicon glyphicon-' + (earned ? 'ok' : 'remove') + '"></span> ';
                            html += '<span' + (earned ? ' class="achievement-earned"' : '') + ' rel="tooltip" title="' + desc + '">' + desc + '</span>';
                            html += '</div>';
                        }
                        html += '</td></tr>';
                    }
                    // Per character
                    else {
                        var charCompleted = {},
                            charCriteria = {};
                        for (var i = 0; i < this.cheev.criteria.length; i++) {
                            var criteriaID = this.cheev.criteria[i][0],
                                requiredQuantity = this.cheev.criteria[i][1],
                                criteria = WoWthing.data.achievement_criteria[criteriaID],
                                crits = userdata.criteria[criteriaID],
                                desc = format_criteria(criteria)[1];

                            if (crits) {
                                for (var j = 0; j < crits.length; j++) {
                                    var characterID = crits[j][0],
                                        amount = crits[j][1];

                                    charCriteria[characterID] = (charCriteria[characterID] || {});

                                    //console.log(characterID, amount, requiredQuantity, crits);

                                    if (check_criteria(criteriaID, criteria, requiredQuantity, characterMap[characterID])) {
                                        charCompleted[characterID] = (charCompleted[characterID] || 0) + 1;
                                        charCriteria[characterID][criteriaID] = true;
                                    }
                                }
                            }
                        }

                        var temp = [];
                        for (var k in charCompleted) {
                            if (charCompleted[k] === 0) {
                                continue;
                            }

                            var character = characterMap[k];
                            if (character === undefined) {
                                continue;
                            }

                            if (this.cheev.check_faction(character)) {
                                temp.push([
                                    characterMap[k],
                                    charCompleted[k],
                                ]);
                            }
                        }

                        if (temp.length > 0) {
                            temp.sort(function(a, b) {
                                if (a[1] < b[1]) { return 1; }
                                else if (a[1] > b[1]) { return -1; }

                                if (a[0].name < b[0].name) { return -1; }
                                else if (a[0].name > b[0].name) { return 1; }
                                else { return 0; }
                            });

                            for (var i = 0; i < Math.min(3, temp.length); i++) {
                                var character = temp[i][0],
                                    value = temp[i][1];

                                //console.log(this.cheev);

                                var parts = format_criteria({type: 0}, value, (this.cheev.requiredCriteria || this.cheev.criteria.length));
                                var per = parts[0],
                                    text = parts[1];

                                html += '<tr><td class="achievement-criteria" colspan="3">';
                                for (var j = 0; j < this.cheev.criteria.length; j++) {
                                    var criteriaID = this.cheev.criteria[j][0],
                                        requiredQuantity = this.cheev.criteria[j][1],
                                        criteria = WoWthing.data.achievement_criteria[criteriaID],
                                        crits = userdata.criteria[criteriaID],
                                        desc = format_criteria(criteria)[1];

                                    var earned = (crits && charCriteria[character.id][criteriaID]);

                                    html += '<div class="achievement-criteria-box col-lg-4 col-md-6">';
                                    html += '<span class="glyphicon glyphicon-' + (earned ? 'ok' : 'remove') + '"></span> ';
                                    html += '<span' + (earned ? ' class="achievement-earned"' : '') + ' rel="tooltip" title="' + desc + '">' + desc + '</span>';
                                    html += '</div>';
                                }
                                html += '</td></tr>';

                                html += '<tr><td class="achievement-criteria" colspan="3">';
                                html += '<div class="progress"><div class="progress-bar progress-bar-success" style="width: ' + per + '%">';
                                html += '<span>' + text + '  (' + character.name + '-' + character.realm + ')</span></div></div>';
                                html += '</td></tr>';
                            }
                        }
                        else {
                            html += '<tr><td class="achievement-criteria" colspan="3">';
                            for (var i = 0; i < this.cheev.criteria.length; i++) {
                                var criteriaID = this.cheev.criteria[i][0],
                                    requiredQuantity = this.cheev.criteria[i][1],
                                    criteria = WoWthing.data.achievement_criteria[criteriaID],
                                    desc = format_criteria(criteria)[1];

                                html += '<div class="achievement-criteria-box col-lg-4 col-md-6">';
                                html += '<span class="glyphicon glyphicon-remove"></span> <span rel="tooltip" title="' + desc + '">' + desc + '</span></div>';
                            }
                            html += '</td></tr>';
                        }
                    }
                }
            }

            html += '</tr>';

            return new Handlebars.SafeString(html);
        });

        // Currencies
        Handlebars.registerHelper('currenciesCurrency', function() {
            var currency = this[0],
                checked = userdata.profile.hide_currencies.indexOf(currency.id) >= 0 ? ' checked="checked"' : '';

            //var html = '<td class="currencies-value" rel="tooltip" title="' + currency.name + '">';
            var html = '<td class="currencies-value" data-tippy-content="' + currency.name + '">';
            html += '<label><input type="checkbox" name="hide_' + currency.id + '"' + checked + '>';
            //html += WoWthing.util.get_icon_img(currency.icon, 16);
            html += WoWthing.util.get_openwow_icon(currency.icon, 16);
            html += '</label></td>';
            return new Handlebars.SafeString(html);
        });

        Handlebars.registerHelper('currenciesCurrencies', function(currencies) {
            var html = '';
            for (var i = 0; i < currencies.length; i++) {
                var currency = this.currencies[currencies[i][0].id],
                    hide = currencies[i][1];

                var classes = ["currencies-value"];
                if (currency && currency.have_max > 0) {
                    var per = currency.have_value / currency.have_max * 100;
                    if (per >= 100) {
                        classes.push("text-danger");
                    }
                    else if (per >= 70) {
                        classes.push("text-warning");
                    }
                }

                var niceValue = WoWthing.util.commas(currency ? currency.have_value : 0);

                //html += '<td class="' + classes.join(' ') + '" rel="tooltip" title="' + niceValue;
                html += '<td class="' + classes.join(' ') + '" data-tippy-content="' + niceValue;
                if (currency && currency.have_max > 0) {
                    html += ' / ' + WoWthing.util.commas(currency.have_max);
                }
                html += ' ' + currencies[i][0].name + '"' + (hide ? ' style="display:none"' : '') + '>' + niceValue;
                //html += WoWthing.util.get_icon_img(currencies[i][0].icon, 16);
                html += WoWthing.util.get_openwow_icon(currencies[i][0].icon, 16);
                html += '</td>';
            }
            return new Handlebars.SafeString(html);
        });

        Handlebars.registerHelper('currencyIcon', function(currencyId) {
            var icon = WoWthing.data.currencies[currencyId].icon;
            return new Handlebars.SafeString(WoWthing.util.get_openwow_icon(icon, 16));
        });

        // Gear
        Handlebars.registerHelper('gearItems', function() {
            var html = '';

            for (var i = 0; i < WoWthing.data.slot_order.length; i++) {
                var slot = WoWthing.data.slot_order[i],
                    item = this.equipped[slot];

                html += '<td class="gear-item">';
                if (item !== undefined) {
                    html += WoWthing.util.get_equipped_item(item, slot, item.quality, userdata.profile.gear_show_item_level ? item.level : undefined, this.cls);
                }
                html += '</td>';
            }

            // Show bags?
            if (userdata.profile.gear_show_bags) {
                var bags = this.bags[1] || {};
                for (var i = 1; i <= 4; i++) {
                    html += '<td class="gear-item' + (i === 1 ? ' gear-first-bag' : '') + '">';
                    if (bags[i] !== undefined) {
                        html += '<div class="icon-overlay">';
                        html += '<a href="https://' + WoWthing.data.wowdb_base + '/items/' + bags[i] + '">';
                        html += WoWthing.util.get_id_img("items", bags[i], 36);
                        html += '<span class="br">' + WoWthing.data.bag_slots[bags[i]] + '</span>';
                        html += '</a></div>';
                    }
                    html += '</td>';
                }
            }

            return new Handlebars.SafeString(html);
        });

        // Lockouts
        Handlebars.registerHelper('lockoutDifficulty', function(difficulty) {
            return WoWthing.data.difficulties[difficulty];
        });
        Handlebars.registerHelper('lockoutsLockouts', function(instanceNames) {
            var html = '',
                now = moment();

            for (var i = 0; i < instanceNames.length; i++) {
                var short_name = instanceNames[i][0],
                    instance = instanceNames[i][1],
                    difficulty = instanceNames[i][2],
                    flop = instanceNames[i][3],
                    found = false;

                html += '<td class="lockouts-value' + (flop ? ' lockouts-alt' : '') + '">';

                if (this.lockouts[instance.id]) {
                    for (var j = 0; j < this.lockouts[instance.id].length; j++) {
                        var lockout = this.lockouts[instance.id][j],
                            expired = (lockout.expires === null || lockout.expires < now),
                            incomplete = (lockout.killed < lockout.total);

                        if (WoWthing.data.split_lockouts[instance.id] === true && lockout.difficulty !== difficulty) {
                            continue;
                        }

                        // Don't show expired non-raids
                        if (expired && [1, 2, 8, 11, 12, 23].indexOf(lockout.difficulty) >= 0) {
                            continue;
                        }

                        // Don't show expired old raids
                        if (userdata.profile.lockouts_ignore_old_expired_raids && expired && instance.expansion < WoWthing.data.current_expansion) {
                            continue;
                        }

                        var difficultyShort = WoWthing.data.difficulty_short[lockout.difficulty],
                            instanceName = instance.name + (difficultyShort ? ' <a>[' + difficultyShort + ']</a>' : '');

                        html += '<span class="icon-' + (expired ? 'clock' : (incomplete ? 'star-half' : 'star')) + '"';
                        html += ' title="' + instanceName + (lockout.total > 0 ? ' (' + lockout.killed + '/' + lockout.total + ')' : ' (??/??)');
                        html += '" data-content="' + lockout_popover(lockout) + '" rel="popover"></span>';

                        found = true;
                        break;
                    }
                }
                else if (this.level === WoWthing.data.max_player_level && WoWthing.data.mythic_dungeons.indexOf(instance.id) >= 0) {
                    var instanceName = instance.name + ' <a>[' + WoWthing.data.difficulty_short[23] + ']</a>';
                    html += '<span class="icon-cancel"' + WoWthing.util.fancy_tooltip(lockout_popover(null), instanceName) + '></span>';
                }

                html += '</td>';
            }

            return new Handlebars.SafeString(html);
        });

        // Progress
        Handlebars.registerHelper('progressProgress', function(data) {
            var html = '';

            for (var i = 0; i < data.length; i++) {
                var section = data[i][1];
                for (var j = 0; j < section.length; j++) {
                    var progress = section[j],
                        popover = '<ul class=\'list-unstyled progress-popover\'>',
                        popover_extra = '',
                        name = '',
                        icon = '',
                        things = [],
                        doneCount = 0,
                        totalCount = 0;

                    if (progress.level > this.level) {
                        html += '<td class="progress-text' + (things.length < 10 ? '1' : '2') + '"></td>';
                        continue;
                    }

                    if (this.side === 'alliance') {
                        name = progress.alliance_name || progress.name;
                        icon = progress.alliance_icon || progress.icon;
                        things = progress.alliance_things || progress.things;
                    }
                    else if (this.side === 'horde') {
                        name = progress.horde_name || progress.name;
                        icon = progress.horde_icon || progress.icon;
                        things = progress.horde_things || progress.things;
                    }

                    if (progress.level) {
                        name = name + ' [<span class=\'yellowish\'>' + progress.level + '+</span>]';
                    }
                    things = (things.constructor === Array ? things : things[this.cls]);

                    if (progress.type === 'research') {
                        totalCount = 8;
                        popover = popover.replace('progress-popover', 'progress-research-popover');

                        var research = WoWthing.data.garrison_talents[this.cls];
                        for (var k = 0; k < 8; k++) {
                            var tier = research[k],
                                have = undefined;
                            
                            popover += '<li><div>';
                            for (var l = 0; l < tier.length; l++) {
                                var talent = tier[l],
                                    finishes = this.order_hall_talents[talent.id];
                                if (finishes !== undefined) {
                                    doneCount++;
                                    have = talent;
                                }
                                popover += WoWthing.util.get_icon_img(talent.icon, 36, have === talent ? 'have' : 'nohave').replace(/"/g, "'");
                            }
                            popover += '</div></li>';

                            if (have !== undefined) {
                                var finishes = this.order_hall_talents[have.id],
                                    in_progress = (finishes !== undefined && finishes > 0);

                                if (in_progress) {
                                    popover += '<li class=\'researching\'>' + have.name + '</li>';
                                    popover += '<li class=\'exciting\'>' + moment.unix(finishes).format('YYYY-MM-DD h:mm a') + '</li>';
                                }
                                else {
                                    popover += '<li class=\'highlight\'>' + have.name + '</li>';
                                }
                            }
                            else {
                                popover += '<li class=\'warning\'>No talent researched!</li>';
                            }
                        }
                    }
                    else {
                        doneCount = 0;
                        totalCount = things.length;

                        for (var k = 0; k < things.length; k++) {
                            // id, name
                            var thing = things[k],
                                done = false,
                                text = thing[1],
                                sub_text = undefined;

                            if (typeof thing[0] === 'string') {
                                if (thing[0] === '-') {
                                    popover += '<hr>';
                                }
                                else if (thing[0].startsWith('#')) {
                                    popover += '<li class=\'section\'>' + thing[0].replace('#', '') + '</li>';
                                }
                                totalCount--;
                                continue;
                            }

                            if (progress.type === 'mixed') {
                                if (thing[0][0] === 'achievement') {
                                    done = (userdata.achievements[thing[0][1]] !== undefined);
                                    if (thing[2] !== undefined) {
                                        sub_text = thing[2];
                                    }
                                }
                                else if (thing[0][0] === 'fishing') {
                                    
                                }
                                else if (thing[0][0] === 'level') {
                                    text = progress_replace(text, this.level, thing[2]);
                                    done = (this.level >= thing[2]);
                                }
                                else if (thing[0][0] === 'mythic15') {
                                    done = this.balance_mythic15;
                                    sub_text = thing[2];
                                }
                                else if (thing[0][0] === 'quest') {
                                    done = (this.quests.indexOf(thing[0][1]) >= 0);
                                    if (thing[2] !== undefined) {
                                        sub_text = thing[2];
                                    }
                                }
                                else if (thing[0][0] === 'data') {
                                    var char_has = this[thing[0][1]];
                                    text = progress_replace(text, char_has.length, thing[2]);
                                    done = (char_has.length >= thing[2]);

                                    if (!done) {
                                        var all_things = WoWthing.data.progress_data[thing[0][1]];
                                        for (var m = 0; m < all_things.length; m++) {
                                            var thing_id = all_things[m][0],
                                                thing_name = all_things[m][1];

                                            popover_extra += '<li class=\'indent\'><span class=\'icon-' + (char_has.indexOf(thing_id) >= 0 ? 'ok highlight' : 'cancel warning') + '\'></span> ';
                                            popover_extra += thing_name + '</li>';
                                        }
                                    }
                                }
                            }
                            else {
                                if (progress.type === 'quest') {
                                    for (var l = 0; l < thing[0].length; l++) {
                                        if (this.quests.indexOf(thing[0][l]) >= 0) {
                                            done = true;
                                            break;
                                        }
                                    }
                                    if (thing[2] !== undefined) {
                                        sub_text = thing[2];
                                    }
                                }
                                else if (progress.type === 'character') {
                                    var value = this[thing[0][0]] || 0;
                                    text = progress_replace(text, value, thing[2]);
                                    done = (value >= thing[2]);
                                }
                            }

                            if (done) {
                                doneCount++;
                            }
                            if (text.startsWith('*')) {
                                text = '<b>' + text.substr(1) + '</b>';
                            }

                            popover += '<li><span class=\'icon-' + (done ? 'ok highlight' : 'cancel warning') + '\'></span> ' + text + '</li>';
                            if (popover_extra !== '') {
                                popover += popover_extra;
                                popover_extra = '';
                            }
                            if (sub_text !== undefined) {
                                popover += '<li class=\'artifact-prestige\'><span class=\'icon-ok\' style=\'opacity:0\'></span> <i>(' + thing[2] + ')</i></li>';
                            }
                        }
                    }
                    popover += '</ul>';

                    var doneClass = (doneCount === totalCount ? 'highlight' : (doneCount === 0 ? 'warning' : 'quality7'));
                    html += '<td class="progress-text-' + (totalCount < 10 ? '1' : '2') + '" ' + WoWthing.util.fancy_tooltip(popover, name) + '>';
                    html += WoWthing.util.get_icon_img(icon, 16);
                    html += '<span class="' + doneClass + '">' + doneCount + '/' + totalCount + '</span>';
                    html += '</td>';
                }
            }

            if (html === '') {
                html = '<td></td>';
            }

            return new Handlebars.SafeString(html);
        });

        // Reputations
        Handlebars.registerHelper('reputationsIcon', function() {
            var paragon = WoWthing.data.paragon_rewards[this.alliance_id || this.id],
                html = '',
                popover = '',
                icon = '';

            if (paragon !== undefined) {
                var allParagon = paragon.concat(WoWthing.data.paragon_rewards[this.horde_id] || []);
                allParagon.sort(function (a, b) {
                    if (a[0] < b[0]) { return -1; }
                    else if (a[0] > b[0]) { return 1; }

                    if (a[2] < b[2]) { return -1; }
                    else if (a[2] > b[2]) { return 1; }

                    return 0;
                });

                paragon = [allParagon[0]];
                for (var i = 1; i < allParagon.length; i++) {
                    if (allParagon[i][2] !== allParagon[i-1][2]) {
                        paragon.push(allParagon[i]);
                    }
                }

                popover = '<ul class=\'list-unstyled reputation-popover\'>';
                var have_count = 0;
                for (var i = 0; i < paragon.length; i++) {
                    var thing_type = paragon[i][0],
                        thing_id = paragon[i][1],
                        thing_name = paragon[i][2],
                        have = false;

                    if (
                        (thing_type === 'mount' && userdata.accounts[0].mounts[thing_id]) ||
                        (thing_type === 'pet' && userdata.accounts[0].pets[thing_id]) ||
                        (thing_type === 'toy' && userdata.accounts[0].toys[thing_id])
                    )
                    {
                        have = true;
                        have_count++;
                    }

                    popover += '<li><span class=\'icon-' + (have ? 'ok' : 'cancel') + '\'></span> ' + thing_name + ' <span>(';
                    popover += thing_type.charAt(0).toUpperCase() + thing_type.substr(1) + ')</span></li>';
                }

                popover += '</ul>';

                if (have_count === paragon.length) {
                    icon = 'ok';
                }
                else if (have_count > 0) {
                    icon = 'star-half';
                }
                else {
                    icon = 'cancel';
                }
            }
            else if (this.note !== undefined) {
                popover = this.note;
            }
            else {
                popover = 'No paragon rewards';
            }

            // Split icons, hoo boy
            if (this.alliance_id !== undefined) {
                html += '<div class="icon-split icon-split-36"' + WoWthing.util.fancy_tooltip(popover, this.alliance_name + ' / ' + this.horde_name) + '>';
                if (this.bodyguard === true) {
                    html += WoWthing.util.get_garrison_img(this.alliance_icon, 'faction0');
                    html += WoWthing.util.get_garrison_img(this.horde_icon, 'faction1');
                }
                else {
                    html += WoWthing.util.get_icon_img(this.alliance_icon, 36, 'faction0');
                    html += WoWthing.util.get_icon_img(this.horde_icon, 36, 'faction1');
                }
            }
            else {
                html += '<div' + WoWthing.util.fancy_tooltip(popover, this.name) + '>';
                if (this.bodyguard === true) {
                    html += WoWthing.util.get_garrison_img(this.icon);
                }
                else {
                    html += WoWthing.util.get_icon_img(this.icon, 36);
                }
            }

            if (icon !== '') {
                html += '<span class="icon-' + icon + '"></span>';
            }
            html += '</div>';
            return new Handlebars.SafeString(html);
        });
        Handlebars.registerHelper('reputationsReputations', function(repGroups) {
            var html = '';
            for (var i = 0; i < repGroups.length; i++) {
                var reps = repGroups[i];
                for (var j = 0; j < reps.length; j++) {
                    var rep = reps[j],
                        id,
                        name;

                    if (this.side === 'alliance') {
                        id = rep.alliance_id || rep.id;
                        name = rep.alliance_name || rep.name;
                    }
                    else {
                        id = rep.horde_id || rep.id;
                        name = rep.horde_name || rep.name;
                    }

                    var charRep = (this.repMap || {})[id];

                    html += '<td class="reputations-rep"';
                    if (charRep !== undefined) {
                        var levelName = charRep.getLevelName(rep),
                            popover = '',
                            cssClass = '',
                            per;

                        if (rep.bodyguard === true) {
                            cssClass = 'quality' + (charRep.level + 2);
                            per = (charRep.reputation_max > 0 ? (charRep.reputation_value / charRep.reputation_max * 100).toFixed(1) : 100);
                        }
                        else if (rep.friend === true) {
                            cssClass = 'quality' + charRep.level;
                            per = (charRep.reputation_max > 0 ? (charRep.reputation_value / charRep.reputation_max * 100).toFixed(1) : 100);
                        }
                        else {
                            if (rep.paragon === true && charRep.level === 7 && charRep.paragon_max > 0) {
                                cssClass = 'quality5';
                                per = (((charRep.paragon_ready ? charRep.paragon_max : 0) + charRep.paragon_value) / charRep.paragon_max * 100).toFixed(1);

                                popover = WoWthing.util.commas(charRep.paragon_value) + ' / ' + WoWthing.util.commas(charRep.paragon_max) + ' Paragon';
                                if (charRep.paragon_ready) {
                                    cssClass = 'exciting';
                                    popover += '<br><br><b>REWARD READY!</b>';
                                }
                            }
                            else {
                                if (charRep.level >= 3) {
                                    cssClass = 'quality' + (charRep.level - 3);
                                }
                                else {
                                    cssClass = 'badrep' + charRep.level;
                                }
                                per = (charRep.reputation_max > 0 ? (charRep.reputation_value / charRep.reputation_max * 100).toFixed(1) : 100);
                            }
                        }

                        if (popover === '') {
                            popover = WoWthing.util.commas(charRep.reputation_value) + ' / ' + WoWthing.util.commas(charRep.reputation_max) + ' ' + levelName;
                        }

                        html += WoWthing.util.fancy_tooltip(popover, name) + '>';
                        html += '<span class="' + cssClass + '">' + per + '%' + '</span';
                    }
                    html += '></td>';
                }
            }
            return new Handlebars.SafeString(html);
        });

        // Settings
        Handlebars.registerHelper('settingsCharacter', function() {
            var html = WoWthing.util.character_race_icon(this) + WoWthing.util.character_class_icon(this);
            html += this.name;
            return new Handlebars.SafeString(html);
        });
    };

    var progress_replace = function(text, value, required) {
        text = text.replace('{0}', '<span class=\'artifact-prestige\'>' + Math.min(value, required) + '</span>');
        text = text.replace('{1}', '<span class=\'artifact-prestige\'>' + required + '</span>');
        return text;
    };

    var load_data = function() {
        $.ajax({
            url: WoWthingState.userdata_url,
            success: function(data, textStatus, jqXHR) {
                loading = false;
                userdata = data;

                cache.home_loading.hide();

                // Do some userdata stuff
                userdata_stuff();

                // Update sidebar
                update_sidebar();

                // Update hash
                loadHash = window.location.hash || '';
                var hash = (window.location.hash || '#overview').split(':')[0];
                $('#home-sidebar a[href="' + hash + '"]').click();
            },
            dataType: 'json',
        });
    };

    var userdata_stuff = function() {
        // Grouping stuff
        var groupTemp = userdata.profile.overview_group.split(',');
        if (groupTemp[0] === 'default') {
            groupTemp = ['realm'];
        }
        overviewGrouping = groupTemp;

        // Sort order stuff
        var sortTemp = userdata.profile.overview_sort.split(',');
        if (sortTemp[0] === 'default') {
            sortTemp = ['-level', 'name'];
        }

        for (var i = 0; i < sortTemp.length; i++) {
            var thing = sortTemp[i],
                invert = false;
            if (thing.startsWith('-')) {
                thing = thing.replace('-', '');
                invert = true;
            }
            sortOrder.push([thing, invert]);
        }
        //sortOrder.unshift(['realm', false]);

        // Group professions by parent
        var groupedProfessions = {};
        for (var k in WoWthing.data.professions) {
            var profession = WoWthing.data.professions[k],
                parent = profession.parent || profession.id;
            groupedProfessions[parent] = (groupedProfessions[parent] || []);
            groupedProfessions[parent].push(profession.id);
        }
        for (var k in groupedProfessions) {
            groupedProfessions[k].sort(function(a, b) {
                var a_tier = WoWthing.data.professions[a].tier,
                    b_tier = WoWthing.data.professions[b].tier;
                return b_tier - a_tier;
            });
        }
        WoWthing.data.grouped_professions = groupedProfessions;

        var realms = {};
        userReputations = {};
        userdata._max_workorders = 0;
        userdata._total_played = 0;
        userdata._total_gold = 0;
        userdata._pvp_prestige = 0;
        userdata._pvp_level = 1;
        userdata._tag_count = (userdata.tags ? WoWthing.util.keys(userdata.tags).length : 0);

        for (var i = 0; i < userdata.accounts.length; i++) {
            var account = userdata.accounts[i];

            account.quests = WoWthing.util.decode_base64_uint16_array(account.quests);
            userdata.achievements = WoWthing.util.decode_achievements(account.achievements);

            //userdata.achievements = JSON.parse(account.achievements) || {};
            userdata.criteria = JSON.parse(account.criteria) || {};

            for (var j in account.quests) {
                userQuests[account.quests[j]] = true;
            }

            // Convert lists of things to objects for faster lookup
            var mounts = JSON.parse(account.mounts) || [];
            account.mounts_count = mounts.length;
            account.mounts = {};
            for (var j = 0; j < mounts.length; j++) {
                account.mounts[mounts[j]] = true;
            }
            
            var pets = JSON.parse(account.pets) || [];
            account.pets_count = pets.length;
            account.pets = {};
            for (var j = 0; j < pets.length; j++) {
                var pet = pets[j];
                if (account.pets[pet.i] === undefined) {
                    account.pets[pet.i] = [];
                }
                account.pets[pet.i].push(new CharacterPet(pet));
            }

            var toys = account.toys;
            account.toys_count = toys.length;
            account.toys = {};
            for (var j = 0; j < toys.length; j++) {
                account.toys[toys[j]] = true;
            }
        }

        var nameCount = {};
        for (var i = 0; i < userdata.characters.length; i++) {
            var character = userdata.characters[i];
            characterMap[character.id] = character;

            // Secret names
            if (character.name === 'SECRET') {
                nameCount[character.realm_long] = nameCount[character.realm] || {};

                var cls = WoWthing.data.classes[character.cls].name,
                    gender = (character.gender === 0 ? 'Mr' : 'Mrs');
                var name = /*gender +*/ cls.replace(' ', '');

                nameCount[character.realm_long][name] = (nameCount[character.realm_long][name] || 0) + 1;
                var n = nameCount[character.realm_long][name];
                if (n > 1) {
                    name = name + ' ' + n;
                }
                character.name = name;
            }

            // Hidden status
            character.hidden = (userdata.profile.hide_characters.indexOf(character.id) >= 0);
            character.hiddenLevel = (character.hidden || character.level <= 10);
            character.hiddenOrderHall = (character.hidden || character.level < 101);

            // Side (faction)
            character.side = WoWthing.data.races[character.race].side;

            // Currencies
            var currency_list = character.currencies;
            character.currencies = {};
            for (var j = 0; j < currency_list.length; j++) {
                var currency = new CharacterCurrency(currency_list[j]);
                character.currencies[currency.id] = currency;
            }

            // Equipped items
            var equipped = {},
                items = character.equipped || [],
                total_ilvl = 0;
            for (var j = 0; j < items.length; j++) {
                var eq = items[j];
                // Skip shirts and tabards
                if (eq.s === 4 || eq.s === 19) {
                    continue;
                }

                var extra = JSON.parse(eq.e);

                equipped[eq.s] = {
                    item_id: eq.i,
                    level: eq.l,
                    quality: eq.q,
                    context: eq.c,
                    azerite: extra.a,
                    bonuses: extra.b,
                    enchant: extra.e,
                    gems: extra.g.replace(/:/g, ','),
                    relics: extra.r,
                    upgrade: extra.u,
                };
                total_ilvl += equipped[eq.s].level;
            }
            // Two-handed needs to be counted twice
            if (equipped[16] && !equipped[17]) {
                total_ilvl += equipped[16].level;
            }

            character.equipped = equipped;

            // Average ilvl
            character.average_ilvl = (total_ilvl / 16).toFixed(1);

            // Lockouts
            for (var k in character.lockouts) {
                var lockouts = character.lockouts[k];
                for (var j = 0; j < lockouts.length; j++) {
                    if (lockouts[j].expires !== null) {
                        lockouts[j].expires = moment(lockouts[j].expires);
                    }
                }
            }

            // Group professions by parent, ugh
            var groupedProfessions = {};
            for (var j = 0; j < character.professions.length; j++) {
                var profession = character.professions[j],
                    professionData = WoWthing.data.professions[profession.id],
                    parentID = professionData.parent || profession.id;
                
                profession.is_maxed = (profession.current_skill >= profession.max_skill);
                profession.profession = WoWthing.data.professions[profession.id];

                groupedProfessions[parentID] = (groupedProfessions[parentID] || []);
                groupedProfessions[parentID].push(profession);
            }

            // Sort grouped professions by expansion, descending
            character.professions = [];
            for (var k in groupedProfessions) {
                groupedProfessions[k].sort(function(a, b) {
                    var a_tier = WoWthing.data.professions[a.id].tier,
                        b_tier = WoWthing.data.professions[b.id].tier;
                    return b_tier - a_tier;
                });
                character.professions.push(groupedProfessions[k]);
            }

            // Professions
            character.professions.sort(function(a, b) {
                var a_name = a[0].profession[character.side + '_name'],
                    b_name = b[0].profession[character.side + '_name'];
                return a_name.localeCompare(b_name);
            });

            if (character.professions[0]) {
                character.profession1 = character.professions[0];
            }
            if (character.professions[1]) {
                character.profession2 = character.professions[1];
            }

            // Quests
            character.quests = WoWthing.util.decode_base64_uint16_array(character.quests);

            // Reputations
            character.repMap = {};
            if (!character.hidden) {
                for (var j = 0; j < character.reputations.length; j++) {
                    var rep = new CharacterReputation(character.reputations[j]);
                    character.repMap[rep.faction_id] = rep;
                    //if (character.name == 'Keikuro') { console.log(character.reputations[j], rep); }
                    
                    //if (rep !== 0) {
                        userReputations[rep.faction_id] = userReputations[rep.faction_id] || [];
                        userReputations[rep.faction_id].push([
                            character.id,
                            rep.reputation_current,
                        ]);
                    //}
                }
            }

            // Realm init
            var groupKey = get_grouping_key(character);
            realms[groupKey] = realms[groupKey] || {
                characters: [],
                name: groupKey,
                region: character.region,
                total_gold: 0,
                total_played: 0,
            };

            var realm = realms[groupKey];
            realm.characters.push(character);
            if (!character.hidden) {
                realm.total_gold += character.gold;
                realm.total_played += character.played;
            }
            userdata._total_gold += character.gold;
            userdata._total_played += character.played;
        }

        var temp = [];
        for (var k in realms) {
            realms[k].characters.sort(overview_character_sort);
            temp.push([k, realms[k]]);
        }
        temp.sort();

        for (var i = 0; i < temp.length; i++) {
            temp[i][1].id = i;
            realmList.push(temp[i][1]);
        }

        // Filter out hidden crap
        for (var i = 0; i < realmList.length; i++) {
            var realm = realmList[i],
                shown = false;

            realm.allHiddenLevel = true;
            for (var j = 0; j < realm.characters.length; j++) {
                var character = realm.characters[j];
                if (!character.hidden) {
                    shown = true;
                    //break;
                }
                if (!character.hiddenLevel) {
                    realm.allHiddenLevel = false;
                }
            }

            if (shown) {
                realmListShow.push(realm);
            }
        }

        // Sort criteria
        for (var k in userdata.criteria) {
            userdata.criteria[k].sort(function(a, b) {
                var an = a[1],
                    bn = b[1];
                if (an < bn) { return 1; }
                else if (an > bn) { return -1; }
                else { return 0; }
            });
        }

        // Sort rep
        for (var k in userReputations) {
            userReputations[k].sort(function(a, b) {
                var alevel = a[1],
                    blevel = b[1];
                if (alevel < blevel) { return 1; }
                else if (alevel > blevel) { return -1; }

                var acur = a[2],
                    bcur = b[2];
                if (acur < bcur) { return 1; }
                else if (acur > bcur) { return -1; }
                else { return 0; }
            });
        }
    };

    var get_grouping_key = function(character) {
        var key_parts = [];
        for (var i = 0; i < overviewGrouping.length; i++) {
            var thing = overviewGrouping[i];
            if (thing === 'realm') {
                key_parts.push(character.realm_long);
            }
            else if (thing === 'tag' && userdata.tags[character.tag_id] !== undefined) {
                key_parts.push(userdata.tags[character.tag_id].long);
            }
            else if (thing === 'faction') {
                key_parts.push(character.side.charAt(0).toUpperCase() + character.side.substr(1));
            }
            else if (thing === 'class') {
                key_parts.push(WoWthing.data.classes[character.cls].name);
            }
        }
        return key_parts.join(' - ');
    };

    var update_sidebar = function() {
        // Count things
        $('#badge-mounts').html(Math.max.apply(null, userdata.accounts.map(function(o) { return o.mounts_count })));
        $('#badge-pets').html(Math.max.apply(null, userdata.accounts.map(function(o) { return o.pets_count })));
        $('#badge-toys').html(Math.max.apply(null, userdata.accounts.map(function(o) { return o.toys_count })));

        // Count lockouts
        var total = 0;
        for (var i = 0; i < userdata.characters.length; i++) {
            var character = userdata.characters[i];
            for (var k in character.lockouts) {
                var lockouts = character.lockouts[k];
                for (var j = 0; j < lockouts.length; j++) {
                    if (lockouts[j].expires !== null) {
                        total++;
                    }
                }
            }
        }

        $('#badge-lockouts').html(total);
    };

    var event_sidebar_click = function(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        if (loading === true) {
            return false;
        }

        // Update errors
        /*if (!WoWthing.home._public) {
            var html = '',
                now = moment();
            for (var i = 0; i < userdata.accounts.length; i++) {
                var account = userdata.accounts[i];
                var expires = moment(account.expires);
                if (!expires || expires.isBefore(now)) {
                    html += '<div class="alert alert-danger">The account <b>' + account.name + '</b> has an expired Battle.net OAuth token!';
                    html += bnet_blurb + '</div>';
                }
                else if (expires && expires.diff(now, 'days') <= 7) {
                    html += '<div class="alert alert-warning">The account <b>' + account.name + '</b> has less than 7 days until the Battle.net OAuth token expires!';
                    html += bnet_blurb + '</div>';
                }
            }
            $('#home-errors')[0].innerHTML = html;
        }*/

        // Fix active thing
        var hash = this.href.split('#')[1];
        if (build[hash] === undefined) {
            return false;
        }

        //cache.home_content.hide();
        //cache.home_settings.hide();

        $('#home-sidebar a').removeClass('active');
        $('#home-sidebar a[href="#' + hash + '"]').addClass('active');

        // Update page hash
        WoWthing.util.update_hash(hash);

        // Call build function
        build[hash]();

        return false;
    };

    ///////////////////////////////////////////////////////////////////////////
    // Overview
    var build_overview = function() {
        // Colspan ugh
        var realm_colspan = 2;
        if (userdata.profile.overview_show_realm) { realm_colspan++; }
        //if (userdata.profile.overview_show_race_icon) { realm_colspan++; }
        //if (userdata.profile.overview_show_class_icon) { realm_colspan++; }
        if (userdata.profile.overview_show_rested) { realm_colspan++; }
        if (userdata.profile.overview_show_ail) { realm_colspan++; }

        var total_colspan = 0;
        if (userdata.profile.overview_show_played) { total_colspan++; }
        if (userdata.profile.overview_show_gold) { total_colspan++; }

        var skip_colspan = 4;
        var currencies_lockouts_colspan = userdata.profile.overview_show_currencies.length + userdata.profile.overview_show_lockouts.length;

        // Now we can do the damn template
        cache.home_content[0].innerHTML = templates.overview({
            currencies_lockouts_colspan: currencies_lockouts_colspan,
            realm_colspan: realm_colspan,
            skip_colspan: skip_colspan,
            total_colspan: total_colspan,
            profile: userdata.profile,
            realms: realmListShow,
            total_gold: userdata._total_gold,
            total_played: userdata._total_played,
        });
        show_content();

        // Activate weird editable things
        var tags = [{value: 0, text: "-NONE-"}];
        for (var k in userdata.tags) {
            tags.push({value: k, text: userdata.tags[k].long});
        }
        /*$('.character-tag').editable({
            name: 'tag_id',
            placement: 'right',
            source: tags,
            success: function(response, new_value) {
                var id = parseInt(this.getAttribute('data-pk'));
                for (var i = 0; i < userdata.characters.length; i++) {
                    var character = userdata.characters[i];
                    if (character.id === id) {
                        character.tag_id = new_value;
                        break;
                    }
                }
                build_overview();
            },
            type: 'select',
            url: '/character_tags/set/',
            params: {csrfmiddlewaretoken: $('input[name="csrfmiddlewaretoken"]').val() },
        });*/

        // Activate tooltips and popovers
        activate_content_stuff();
    };

    ///////////////////////////////////////////////////////////////////////////
    // Achievements
    var build_achievements = function() {
        // If data isn't loaded, do that first
        if (WoWthing.data.achievements === undefined) {
            cache.home_content[0].innerHTML = '<div>Loading data, please wait...</div>';
            show_content();

            $.cachedScript(WoWthing.home._achievement_js, {
                success: build_achievements,
            });
            return;
        }

        var points = rebuild_achievements();

        // Render the template
        cache.home_content[0].innerHTML = templates.achievements({
            categories: WoWthing.data.achievements,
        });
        show_content();

        $('#achievement-summary-completion')[0].innerHTML = WoWthing.util.commas(points);

        // Update button state
        var inputs = $('.js-achievement-input'),
            hash = window.location.hash.replace('#', '');

        if (achievementSettings.earned !== undefined) {
            for (var i = 0; i < inputs.length; i++) {
                var key = inputs[i].id.replace('achievement-', '').replace('-', '_');
                if (achievementSettings[key] === false) {
                    $(inputs[i]).prop('checked', false).change();
                }
            }
        }

        // Hook some events
        $('.js-achievement-category').on('click', achievement_category_click);
        $('.js-achievement-input').on('change', function() {
            achievementSettings = {};
            for (var i = 0; i < inputs.length; i++) {
                var key = inputs[i].id.replace('achievement-', '').replace('-', '_');
                achievementSettings[key] = inputs[i].checked;
            }

            localStorage.setItem('achievementSettings', JSON.stringify(achievementSettings));

            update_achievements();
        });

        // Check the original page hash
        if (loadHash) {
            var categoryID = loadHash.split(':')[1];
            if (categoryID === undefined || categoryID === 'summary') {
                $('#achievements-categories a[data-category="summary"]').click();
            }
            else if (categoryID) {
                var $thing = $('.js-achievement-category[data-category="' + categoryID + '"]');
                var parent = $thing.data('parent');
                if (parent) {
                    $('.js-achievement-category[data-category="' + parent + '"]').click();
                }
                $thing.click();
            }
            loadHash = undefined;
        }
        else {
            $('#achievements-categories a[data-category="summary"]').click();
        }

        WoWthing.checkbox_buttons.go();
        activate_content_stuff();

        //update_achievements();
    };

    var update_achievements = function() {
        var points = rebuild_achievements();
        $('#achievement-summary-completion')[0].innerHTML = WoWthing.util.commas(points);

        // Re-render current category
        var categoryID = window.location.hash.split(':')[1];
        if (categoryID) {
            if (!isNaN(parseInt(categoryID))) {
                render_achievements(categoryID);
            }
            else if (categoryID === 'summary') {
                render_achievements_summary();
            }
        }
    };

    var rebuild_achievements = function() {
        // Calculate some scariness
        var points = 0;
        for (var i = 0; i < WoWthing.data.achievements.length; i++) {
            var category = WoWthing.data.achievements[i];
            do_achievement_category(category);
            points += category._points;
        }
        return points;
    };

    var do_achievement_category = function(cat, update) {
        achievementCategoryMap[cat.id] = cat;

        cat._earned = 0;
        cat._total = 0;
        cat._points = 0;

        var earned = [],
            unearned = [];

        for (var i = 0; i < cat.achievements.length; i++) {
            var achievement = cat.achievements[i];
            achievementMap[achievement.id] = achievement;

            var timestamp = userdata.achievements[achievement.id];

            if ((achievement.faction === 0 && !achievementSettings.alliance) ||
                (achievement.faction === 1 && !achievementSettings.horde)
            ) {
                continue;
            }

            if (timestamp !== undefined) {
                //console.log('not undefined');
                if (timestamp === 0)
                    achievement._timestamp = "[No API timestamp]";
                else
                    achievement._timestamp = moment.unix(timestamp).format('h:mm a YYYY-MM-DD');

                cat._earned++;
                cat._points += achievement.points;

                earned.push(achievement);

                if (timestamp > 0) {
                    if (recentAchievements.length < 10) {
                        recentAchievements.push(achievement);
                    }
                    else if (timestamp > userdata.achievements[recentAchievements[recentAchievements.length - 1].id]) {
                        recentAchievements.push(achievement);
                        recentAchievements.sort(recent_achievements_sort);
                        recentAchievements.splice(10, 5);
                    }
                }
            }
            else {
                unearned.push(achievement);
            }
            cat._total++;
        }

        earned.sort(recent_achievements_sort);

        cat._achievements = earned.concat(unearned);

        var cats = cat.categories || [];
        for (var i = 0; i < cats.length; i++) {
            do_achievement_category(cats[i], update);

            cat._earned += cats[i]._earned;
            cat._total += cats[i]._total;
            cat._points += cats[i]._points;
        }

        cat._percent = Math.floor(cat._earned / cat._total * 100);
        if (isNaN(cat._percent)) {
            cat._percent = 'N/A';
        }
        else {
            cat._percent = cat._percent + '%';
        }

        // Update stuff?
        if (update) {
            var $completion = $('#achievements-categories a[href="#achievements:' + cat.id + '"] .completion');
            $completion[0].innerHTML = cat._percent;
        }
    };

    var recent_achievements_sort = function(a, b) {
        var tsa = userdata.achievements[a.id],
            tsb = userdata.achievements[b.id];

        if (tsa < tsb) { return 1; }
        else if (tsa > tsb) { return -1; }
        else { return 0; }
    };

    var achievement_category_click = function(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        var $this = $(this),
            categoryID = this.getAttribute('data-category'),
            expanded = this.getAttribute('data-expanded'),
            parent = this.getAttribute('data-parent');

        // Set this link as active
        $('#achievements-categories a').removeClass('active');
        $this.addClass('active');

        WoWthing.util.update_hash(this.href.split('#')[1]);

        // Summary click
        if (categoryID === 'summary') {
            render_achievements_summary();
        }
        else {
            // Base category click
            if (parent === null) {
                // Hide all sub-cats
                $('.achievements-subcat').hide();

                // Flip all chevrons
                $('.js-achievement-category .glyphicon').removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-right');

                // Changing category
                if (shownAchievementCategory !== categoryID) {
                    shownAchievementCategory = categoryID;

                    // Show this category's sub-cats
                    $('.achievements-subcat[data-parent="' + categoryID + '"]').show();

                    // Update icon
                    $('.glyphicon', this).removeClass('glyphicon-chevron-right').addClass('glyphicon-chevron-down');
                }
                // Same category
                else {
                    shownAchievementCategory = null;
                }
            }

            render_achievements(categoryID);
        }

        activate_content_stuff();

        return false;
    };

    var render_achievements = function(categoryID) {
        var cat = achievementCategoryMap[categoryID],
            html = '<div class="achievements">';

        for (var i = 0; i < cat._achievements.length; i++) {
            var cheev = cat._achievements[i];
            var earned = (userdata.achievements[cheev.id] !== undefined);

            if ((earned && !achievementSettings.earned) ||
                (!earned && !achievementSettings.not_earned)
            ) {
                continue;
            }

            // Skip chained achievements
            var next = WoWthing.data.achievement_next[cheev.id];
            if (next && userdata.achievements[next]) {
                continue;
            }
            var prev = WoWthing.data.achievement_prev[cheev.id];
            if (prev && !userdata.achievements[prev]) {
                continue;
            }

            html += templates.achievements_achievement({
                cheev: cheev,
                earned: earned,
            });
        }

        html += '</div>';
        $('#achievements-container')[0].innerHTML = html;
    };

    var render_achievements_summary = function() {
        var l = recentAchievements.length;
        for (var i = 0; i < l; i++) {
            recentAchievements.pop();
        }

        var points = rebuild_achievements();

        var all_earned = 0,
            all_total = 0;
        for (var i = 0; i < WoWthing.data.achievements.length; i++) {
            var category = WoWthing.data.achievements[i];
            if (category.name !== 'Feats of Strength' && category.name !== 'Legacy') {
                all_earned += category._earned;
                all_total += category._total;
            }
        }

        $('#achievements-container')[0].innerHTML = templates.achievements_summary({
            all_earned: all_earned,
            all_total: all_total,
            all_percent: (all_earned / all_total * 100).toFixed(1),
            categories: WoWthing.data.achievements,
            recent: recentAchievements,
        });
    };

    var check_criteria = function(criteriaID, criteria, requiredQuantity, character, faction) {
        var known = true,
            valueFunc = null;

        switch (criteria.type) {
            // Profession
            case 7:
                valueFunc = function(assetID, c) {
                    for (var j = 0; j < c.professions.length; j++) {
                        var profSet = c.professions[j];
                        for (var k = 0; k < profSet.length; k++) {
                            var prof = profSet[k];
                            if (prof.id === assetID) {
                                return prof.current_skill;
                            }
                        }
                    }
                    return 0;
                };
                break;

            // Achievement
            case 8:
                return userdata.achievements[criteria.assetID] !== undefined;
            
            // Quest
            case 27:
                return userQuests[criteria.assetID] !== undefined;
            
            // Spell known? Battle Pet/Mount
            // case 34:
            //     return false;
            
            // Have item? Battle Pet/Mount
            // case 36:
            //     return false;
            
            // Reputation
            case 46:
                valueFunc = function(assetID, c) {
                    var rep = c.repMap[assetID];
                    //console.log(assetID, c, rep);
                    return (rep !== undefined ? rep.reputation_current : 0)
                }
                break;
            
            // Battle Pet - vendor? 71942, 72462, 72463, 72464
            case 96:
                return userdata.accounts[0].pets[criteria.assetID] !== undefined;
            
            // Spell known?
            case 34:
            // Item acquired?
            case 36:
            // Battle Pet - dungeon?
            case 155:
            // Battle Pet - world? by name, ugh
            case 157:
            // Beat a specific pet trainer?
            case 158:
                break;

            default:
                known = false;
                break;
        }

        if (!known) {
            console.log('check_criteria dunno', criteriaID, criteria, requiredQuantity);
        }

        if (valueFunc !== null) {
            for (var i = 0; i < userdata.characters.length; i++) {
                var critCharacter = userdata.characters[i],
                    quantity = valueFunc(criteria.assetID, critCharacter);
                //console.log(critCharacter.name, quantity);
                if (check_achievement_faction(critCharacter, faction) &&
                    quantity >= requiredQuantity &&
                    (character === undefined || character.id === critCharacter.id)
                ) {
                    return true;
                }
            }
        }
        else {
            var crits = userdata.criteria[criteriaID] || [];
            for (var i = 0; i < crits.length; i++) {
                var critCharacter = characterMap[crits[i][0]];
                if (critCharacter !== undefined) {
                    if (check_achievement_faction(critCharacter, faction) &&
                        crits[i][1] >= requiredQuantity &&
                        (character === undefined || character.id === critCharacter.id)
                    ) {
                        return true;
                    }
                }
            }
        }
    
        return false;
    }

    var format_criteria = function(criteria, value, requiredQuantity, character) {
        var per, text;

        // Profession
        if (criteria.type === 7) {
            text = WoWthing.data.professions[criteria.assetID].name;
        }
        // Conquest
        // if (criteria.type === 12) {
        //     value = Math.floor(value / 100);
        // }
        // Reputation
        else if (criteria.type === 46 && value !== undefined) {
            if (value >= 42000) {
                text = Math.max(999, value - 42000) + ' / 999 Exalted';
            }
            else if (value >= 21000) {
                text = WoWthing.util.commas(value - 21000) + ' / 21,000 Revered';
            }
            else if (value >= 9000) {
                text = WoWthing.util.commas(value - 9000) + ' / 12,000 Honored';
            }
            else if (value >= 3000) {
                text = WoWthing.util.commas(value - 3000) + ' / 6,000 Friendly';
            }
            else if (value >= 0) {
                text = WoWthing.util.commas(value) + ' / 3,000 Neutral';
            }
            else if (value >= -3000) {
                text = WoWthing.util.commas(value + 3000) + ' / -3,000 Unfriendly';
            }
            else if (value >= -6000) {
                text = WoWthing.util.commas(value + 3000) + ' / -3,000 Hostile';
            }
            else {
                text = WoWthing.util.commas(value + 6000) + ' / -36,000 Hated';
            }
        }
        // Gold
        else if ((criteria.type === 62 || criteria.type === 67) && value !== undefined) {
            text = format_currency(value) + ' / ' + format_currency(requiredQuantity);
        }

        if (value !== undefined && requiredQuantity !== undefined) {
            if (!text) {
                text = WoWthing.util.commas(value) + ' / ' + WoWthing.util.commas(requiredQuantity);
            }
            per = (value / requiredQuantity * 100).toFixed(1);
        }

        if (!text) {
            text = criteria.description;
        }

        return [per, text];
    };

    var format_currency = function(copper) {
        var c = copper % 100,
            s = Math.floor(copper / 100) % 100,
            g = WoWthing.util.commas(Math.floor(copper / 10000)),
            parts = [];

        if (g) {
            parts.push(g + 'g');
        }
        if (s) {
            parts.push(s + 's');
        }
        if (c || parts.length === 0) {
            parts.push(c + 'c');
        }
        return parts.join(' ');
    };

    var check_achievement_faction = function(character, faction) {
        return (
            (faction === 0 && character.side === 'alliance') ||
            (faction === 1 && character.side === 'horde') ||
            faction === 2
        );
    }

    ///////////////////////////////////////////////////////////////////////////
    // Currencies
    var build_currencies = function() {
        var currencies = [],
            count = 0,
            catID = null,
            groups = [];

        for (var i = 0; i < WoWthing.data.currency_list.length; i++) {
            var currencyID = WoWthing.data.currency_list[i],
                currency = WoWthing.data.currencies[currencyID],
                hidden = userdata.profile.hide_currencies.indexOf(currencyID) >= 0;

            if (catID === null || catID !== currency.category) {
                if (count > 0) {
                    groups.push([WoWthing.data.currency_categories[catID], count]);
                }
                catID = currency.category;
                count = 0;
            }

            if (!hidden) {
                count++;
            }

            currencies.push([ WoWthing.data.currencies[currencyID], hidden ]);
        }

        if (count > 0) {
            groups.push([WoWthing.data.currency_categories[catID], count]);
        }

        cache.home_content[0].innerHTML = templates.currencies({
            csrf: $('input[name="csrfmiddlewaretoken"]').val(),
            currencies: currencies,
            groups: groups,
            realm_colspan: 1 + currencies.length,
            realms: realmListShow,
        });
        show_content();

        // Activate tooltips and popovers
        activate_content_stuff();

        $('#currencies-show-hide').on('change', function() {
            $('#currencies-hide-row').toggle();
        });
    };

    ///////////////////////////////////////////////////////////////////////////
    // Gear
    var build_gear = function() {
        // Now we can do the damn template
        cache.home_content[0].innerHTML = templates.gear({
            realm_colspan: 1 + WoWthing.data.slot_order.length,
            realms: realmListShow,
        });
        show_content();

        activate_content_stuff();
    };

    ///////////////////////////////////////////////////////////////////////////
    // Gold history
    var build_gold_history = function() {
        cache.home_content[0].innerHTML = '<div>Loading data, please wait...</div>';
        show_content();

        // Might need to fix gold_history_interval
        if (gold_history_interval !== 'h' && gold_history_interval !== 'd' && gold_history_interval !== 'm')
            gold_history_interval = 'h';

        // If the JS isn't loaded, do that first
        if ($('body').highcharts === undefined) {
            $.cachedScript('/static/js/highcharts.min.js', {
                success: build_gold_history,
            });
            return;
        }

        // If data isn't loaded, do that
        if (!loaded_gold_history) {
            $.ajax({
                url: '/intapi/goldhistory/' + gold_history_interval + '/' + gold_history_period + '/',
                success: function(data, textStatus, jqXHR) {
                    loaded_gold_history = true;

                    // Data is [ "realmName", { "date": gold, ... } ], ... ]

                    // Get all dates
                    var temp = {},
                        max_gold = {};
                    for (var i = 0; i < data.length; i++) {
                        var thing = data[i];
                        for (var k in thing[1]) {
                            temp[k] = true;
                            max_gold[thing[0]] = Math.max(max_gold[thing[0]] || 0, thing[1][k]);
                        }
                    }

                    var dates = [];
                    for (var k in temp) {
                        dates.push(k);
                    }
                    dates.sort();

                    var dateMap = {};
                    for (var i = 0; i < dates.length; i++) {
                        var d = moment.utc(dates[i]).toArray();
                        dateMap[dates[i]] = Date.UTC(d[0], d[1], d[2], d[3], d[4]);
                    }

                    // Do stuff
                    gold_history = [];
                    for (var i = 0; i < data.length; i++) {
                        var name = data[i][0],
                            timeData = data[i][1],
                            last = 0,
                            newData = [];

                        // Skip 0 gold servers
                        if (max_gold[name] === 0) continue;

                        for (var j = 0; j < dates.length; j++) {
                            var value = timeData[dates[j]] || last;
                            last = value;
                            newData.push([
                                dateMap[dates[j]],
                                value,
                            ]);

                            gold_min[name] = Math.min(gold_min[name] || 99999999999, value);
                        }

                        gold_history.push({
                            name: name,
                            data: newData,
                        });
                    }

                    for (var k in gold_min) {
                        gold_min[k] = Math.floor(gold_min[k] / 10000) * 10000;
                    }

                    build_gold_history();
                },
            });
            return;
        }

        // Display a magical chart
        cache.home_content[0].innerHTML = templates.goldhistory();//'<div id="magic-chart"></div>';
        $('#magic-chart').highcharts({
            chart: {
                backgroundColor: '#212529',
                type: 'area',
                events: {
                    redraw: function() {
                        var minValue = 9999999999999;
                        var chart = $('#magic-chart').highcharts();
                        for (var i = 0; i < chart.series.length; i++) {
                            var series = chart.series[i];
                            chartStatus[series.name] = series.visible;
                            if (series.visible) {
                                minValue = Math.min(minValue, gold_min[series.name]);
                            }
                        }
                        localStorage.setItem('chartStatus', JSON.stringify(chartStatus));

                        if (minValue < 9999999999999 && minValue !== chart.yAxis[0].options.min) {
                            chart.yAxis[0].setOptions({
                                min: minValue,
                            });
                            setTimeout(function() {
                                $('#magic-chart').highcharts().redraw();
                            }, 1);
                        }
                    },
                },
            },
            title: {
                text: 'Gold history',
            },
            xAxis: {
                type: 'datetime',
            },
            yAxis: {
                title: {
                    text: 'Gold',
                },
                min: 0,
            },
            tooltip: {
                formatter: function() {
                    var html = '<table class="chart-tooltip-table"><thead><tr><td colspan="2">';
                    html += Highcharts.dateFormat('%A, %b %e, %H:%M', this.x) + '</td></tr></thead><tbody>';
                    for (var i = 0; i < this.points.length; i++) {
                        var point = this.points[i];
                        html += '<tr><td style="color:' + point.series.color + '">' + point.series.name + ':</td>';
                        html += '<td>' + WoWthing.util.commas(point.y) + 'g</td></tr>';
                    }
                    html += '</tbody><tfoot><tr><td>Total:</td><td>' + WoWthing.util.commas(point.total) + 'g</td></tr></tfoot></table>';
                    return html;
                },
                shared: true,
                useHTML: true,
            },
            plotOptions: {
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    },
                },
            },
            series: gold_history,
        });

        // Hide any display items that should be hidden
        var c = $('#magic-chart').highcharts(),
            redraw = false;

        for (var i = 0; i < c.series.length; i++) {
            var series = c.series[i];
            if (chartStatus[series.name] === false) {
                series.setVisible(false, false);
                redraw = true;
            }
        }

        if (redraw) {
            c.redraw();
        }

        // Events
        $('#chart-interval').val(gold_history_interval);
        $('#chart-interval').on('change keyup', function() {
            gold_history_interval = this.value;
            localStorage.setItem('gold_history_interval', gold_history_interval);

            loaded_gold_history = false;
            build_gold_history();
        });
        $('#chart-period').val(gold_history_period);
        $('#chart-period').on('change keyup', function() {
            gold_history_period = this.value;
            localStorage.setItem('gold_history_period', gold_history_period);

            loaded_gold_history = false;
            build_gold_history();
        });
    }

    ///////////////////////////////////////////////////////////////////////////
    // Items
    var load_items = function(callback) {
        if (loaded_items === 0) {
            cache.home_content[0].innerHTML = '<div>Loading data, please wait...</div>';
            show_content();

            loaded_items = 1;

            $.ajax({
                url: '/intapi/useritems/',
                success: function(data, textStatus, jqXHR) {
                    userItems = {};
                    for (var item_id in data.items) {
                        userItems[item_id] = true;
                    }

                    for (var character_id in data.characters) {
                        var charData = data.characters[character_id],
                            character = characterMap[character_id];

                        character.bags = charData.bags;

                        for (var location in charData.items) {
                            character.items[location] = charData.items[location];
                        }
                    }

                    loaded_items = 2;
                    callback();
                },
            });

            return true;
        }
        else {
            return (loaded_items !== 2);
        }
    };

    var build_items = function() {
        if (load_items(build_items))
            return;

        // Add the base template
        cache.home_content[0].innerHTML = templates.items();

        // Add some events
        $('#items-realm').on('change', function() {
            var html = '',
                characters = realmList[this.value].characters;
            for (var i = 0; i < characters.length; i++) {
                var character = characters[i];
                if (!character.hidden) {
                    html += '<option value="' + i + '">' + character.name + '</option>';
                }
            }
            $('#items-character').html(html).change();
        });
        $('#items-character').on('change', function() {
            var character = realmList[$('#items-realm').val()].characters[$('#items-character').val()];

            do_items('items-bags', character, 1);
            do_items('items-bank', character, 2);
            do_items('items-reagent', character, 5);
            do_items('items-void', character, 3);
        });

        // Build the realm select
        var html = '';
        for (var i = 0; i < realmList.length; i++) {
            var all_hidden = true,
                realm = realmList[i];
            for (var j = 0; j < realm.characters.length; j++) {
                var character = realm.characters[j];
                if (character.hidden === false) {
                    all_hidden = false;
                    break;
                }
            }

            if (!all_hidden) {
                html += '<option value="' + i + '">' + realm.name + '</option>';
            }
        }
        $('#items-realm').html(html).change();

        // Show content box
        show_content();
    };

    var do_items = function(target, character, location) {
        var html = '',
            loc_items = character.items[location],
            full_slots = 0,
            total_slots = 0;

        if (loc_items !== undefined) {
            var containers = WoWthing.data.bag_data[location];
            for (var i = 0; i < containers.length; i++) {
                var items = loc_items[containers[i]];
                // Pad out empty bags
                if (items === undefined) {
                    var bagID = (character.bags[location] || {})[containers[i]];
                    if (bagID === undefined) {
                        continue;
                    }

                    items = [];
                    for (var j = 0; j < WoWthing.data.bag_slots[bagID]; j++) {
                        items.push(null);
                    }
                }

                for (var j = 0; j < items.length; j++) {
                    total_slots++;
                    var item = items[j];

                    html += '<div class="items-item' + (item === null || item === 0 ? ' items-missing' : '') + '">';

                    if (item !== null && item !== 0) {
                        full_slots++;
                        html += WoWthing.util.get_item_link(item);
                        html += WoWthing.util.get_id_img("items", item.i, 36);
                        if (item.n > 1) {
                            html += '<span>' + item.n + '</span>';
                        }
                        html += '</a>';
                    }

                    html += '</div>';
                }
            }
        }

        $('#' + target)[0].innerHTML = html;

        var data = full_slots + '/' + total_slots + ' -- ',
            updated;
        if (location === 1) {
            updated = character.last_bag_update;
        }
        else if (location === 3) {
            updated = character.last_void_update;
        }
        else {
            updated = character.last_bank_update;
        }

        data += (updated === null ? 'Never scanned' : 'Scanned ' + moment(updated).fromNow());

        $('#' + target + '-data')[0].innerHTML = data;

        activate_content_stuff();
    };

    ///////////////////////////////////////////////////////////////////////////
    // Lockouts
    var build_lockouts = function() {
        var instances = {},
            instanceMap = {},
            now = moment();

        for (var i = WoWthing.data.current_expansion; i > 0; i--) {
            instanceMap[i] = {};
        }

        for (var i = 0; i < userdata.characters.length; i++) {
            var character = userdata.characters[i];
            for (var k in character.lockouts) {
                var instance = WoWthing.data.instances[k],
                    lockouts = character.lockouts[k],
                    short_name = (WoWthing.data.raid_order_override[k] || '') + instance.short_name;

                lockouts.sort(function(a, b) {
                    if (a.expires === null) return -1;
                    else if (b.expires === null) return 1;

                    if (a.expires < b.expires) return -1;
                    else if (a.expires > b.expires) return 1;

                    return 0;
                });

                for (var j = 0; j < lockouts.length; j++) {
                    var lockout = lockouts[j],
                        expired = (lockout.expires === null || lockout.expires < now);

                    //if (lockout.locked)
                    //    console.log(instance, lockout, expired);

                    // Don't show expired non-raids
                    if (expired && (
                        lockout.difficulty === 1 ||
                        lockout.difficulty === 2 ||
                        lockout.difficulty === 8 ||
                        lockout.difficulty === 11 ||
                        lockout.difficulty === 12 ||
                        lockout.difficulty === 23
                    )) {
                        continue;
                    }

                    // Don't show expired
                    if (userdata.profile.lockouts_ignore_old_expired_raids && expired && instance.expansion < WoWthing.data.current_expansion) {
                        continue;
                    }
                    if (userdata.profile.lockouts_ignore_current_expired_raids && expired && instance.expansion === WoWthing.data.current_expansion) {
                        continue;
                    }

                    var key = k + '-' + lockout.difficulty;
                    if (lockout.expires !== null) {
                        instances[key] = (instances[key] || 0) + 1;
                    }

                    var parts = get_instance_data(instance, lockout.difficulty),
                        instanceName = parts[0],
                        sortOrder = parts[1];

                    instanceMap[instance.expansion][instanceName] = [instance, lockout.difficulty, false, sortOrder, short_name];
                }
            }
        }

        // Pad out current expansion with uncleared mythic dungeons
        var currentExpansion = instanceMap[WoWthing.data.current_expansion];
        for (var i = 0; i < WoWthing.data.mythic_dungeons.length; i++) {
            var instanceID = WoWthing.data.mythic_dungeons[i],
                instance = WoWthing.data.instances[instanceID],
                short_name = (WoWthing.data.raid_order_override[instanceID] || '') + instance.short_name,
                instanceName = get_instance_data(instance, 23)[0];

            if (currentExpansion[instanceName] === undefined) {
                currentExpansion[instanceName] = [instance, 23, null, 0, short_name];
            }
        }
        console.log(instanceMap);

        var expansionHeaders = [],
            instanceNames = [],
            flop = false;

        for (var e = WoWthing.data.current_expansion; e > 0; e--) {
            var temp = [];
            for (var k in instanceMap[e]) {
                temp.push([k].concat(instanceMap[e][k]));
            }

            if (temp.length > 0) {
                flop = !flop;

                for (var i = 0; i < temp.length; i++) {
                    temp[i][3] = flop;
                }

                temp.sort(function(a, b) {
                    if (a[5] < b[5]) { return -1; }
                    else if (a[5] > b[5]) { return 1; }

                    if (a[4] < b[4]) { return 1; }
                    else if (a[4] > b[4]) { return -1; }

                    return 0;
                });
                instanceNames = instanceNames.concat(temp);

                expansionHeaders.push({
                    colspan: temp.length,
                    name: WoWthing.data.expansions[e].name,
                });
            }
        }

        // Now we can do the damn template
        cache.home_content[0].innerHTML = templates.lockouts({
            expansion_headers: expansionHeaders,
            instance_names: instanceNames,
            realm_colspan: 1 + instanceNames.length,
            realms: realmListShow,
        });
        show_content();

        // Activate tooltips and popovers
        activate_content_stuff();
    };

    var get_instance_data = function(instance, difficulty) {
        var instanceName = instance.short_name,
            sortOrder = 0;

        if (WoWthing.data.split_lockouts[instance.id] === true) {
            if (difficulty === 2 || difficulty === 11) {
                instanceName = 'H-' + instanceName;
            }
            else if (difficulty === 3) {
                instanceName += '10';
            }
            else if (difficulty === 4) {
                instanceName += '25';
            }
            else if (difficulty === 5) {
                instanceName += '10H';
            }
            else if (difficulty === 6) {
                instanceName += '25H';
            }
            else if (difficulty === 14) {
                instanceName = 'N-' + instanceName;
                sortOrder = 1;
            }
            else if (difficulty === 15) {
                instanceName = 'H-' + instanceName;
                sortOrder = 2;
            }
            else if (difficulty === 16) {
                instanceName = 'M-' + instanceName;
                sortOrder = 3;
            }
            else if (difficulty === 17) {
                instanceName = 'L-' + instanceName;
            }
        }
        else if (difficulty === 23) {
            instanceName = 'M-' + instanceName;
        }

        return [instanceName, sortOrder];
    }

    ///////////////////////////////////////////////////////////////////////////
    // Reputations
    var build_reputations = function() {
        cache.home_content[0].innerHTML = templates.reputations({
            categories: WoWthing.data.reputations,
        });
        show_content();

        // Hook some events
        $('.js-reputations-category').on('click', reputations_category_click);

        // Check the original page hash
        if (loadHash) {
            var categoryID = loadHash.split(':')[1];
            if (categoryID === undefined) {
                $('#sub-sidebar a')[0].click();
            }
            else if (categoryID) {
                categoryID = categoryID.replace(/\W/g, '');
                var $thing = $('.js-reputations-category[data-category="' + categoryID + '"]');
                $thing.click();
            }
            loadHash = undefined;
        }
        else {
            $('#sub-sidebar a')[0].click();
        }

        activate_content_stuff();
    };

    var reputations_category_click = function(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        var $this = $(this),
            categoryID = this.getAttribute('data-category');

        // Set this link as active
        $('#sub-sidebar a').removeClass('active');
        $this.addClass('active');

        WoWthing.util.update_hash(this.href.split('#')[1]);

        shownReputationCategory = categoryID;
        render_reputations(categoryID);

        activate_content_stuff();

        return false;
    };

    var render_reputations = function(categoryID) {
        for (var i = 0; i < WoWthing.data.reputations.length; i++) {
            var thing = WoWthing.data.reputations[i];
            if (thing.key === categoryID) {
                $('#reputations-content')[0].innerHTML = templates.reputations_section({
                    realms: realmListShow,
                    data: thing,
                });
                break;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Sets
    var build_sets = function() {
        cache.home_content[0].innerHTML = templates.sets();

        update_sets();
        WoWthing.checkbox_buttons.go();

        // Update button state
        var inputs = $('.js-sets-input'),
            hash = window.location.hash.replace('#', '');

        if (set_settings[hash] !== undefined) {
            for (var i = 0; i < inputs.length; i++) {
                var key = inputs[i].id.replace('sets-', '').replace('-', '_');
                if (set_settings[hash][key] === false) {
                    $(inputs[i]).prop('checked', false).change();
                }
            }
        }

        // Events
        $('.js-sets-input').on('change', function() {
            set_settings[hash] = {};
            for (var i = 0; i < inputs.length; i++) {
                var key = inputs[i].id.replace('sets-', '').replace('-', '_');
                set_settings[hash][key] = inputs[i].checked;
            }

            localStorage.setItem('set_settings', JSON.stringify(set_settings));

            update_sets();
        });

        show_content();
    };

    var build_mounts = build_sets;
    var build_pets = build_sets;
    var build_toys = build_sets;

    var update_sets = function() {
        // FIXME: hardcoded account 0
        var hash = window.location.hash.replace('#', '');
        var have = {},
            haveMap = {},
            thing_sets = WoWthing.data[hash],
            sets = [],
            section,
            section_name = '',
            section_data = {},
            section_have = 0,
            section_shown = 0,
            total_have = 0,
            total_shown = 0,
            found_alliance = false,
            found_horde = false,
            unique_have = [],
            unique_shown = [];

        var best = 0;
        for (var i = 0; i < userdata.accounts.length; i++) {
            var count = userdata.accounts[i][hash + '_count'];
            if (count > best) {
                best = count;
                have = userdata.accounts[i][hash];
            }
        }

        // Check settings
        var settings = set_settings[hash];
        if (settings === undefined) {
            settings = {
                collected: $('#sets-collected')[0].checked,
                not_collected: $('#sets-not-collected')[0].checked,
                alliance: $('#sets-alliance')[0].checked,
                horde: $('#sets-horde')[0].checked,
                unavailable: $('#sets-unavailable')[0].checked,
                chaching: $('#sets-chaching')[0].checked,
            };
            set_settings[hash] = settings;
        }

        for (var i = 0; i < thing_sets.length; i++) {
            var set_name = thing_sets[i][0],
                set_things = thing_sets[i][1];

            found_alliance = found_alliance || (set_name.indexOf('Alliance') >= 0 || set_name.indexOf('Both') === 0);
            found_horde = found_horde || (set_name.indexOf('Horde') >= 0 || set_name.indexOf('Both') === 0);

            var temp = {
                have_all: true,
                name: thing_sets[i][0],
                things: [],
            };
            temp.new_section = temp.name.startsWith('#');
            if (temp.new_section)
            {
                temp.name = temp.name.replace('#', '');
                temp.slug = temp.name.toLowerCase().replace(/[ \/]/g, '_').replace(/\$/g, 'dollar');

                if (section) {
                    section_data[section] = [section_have, section_shown];
                }

                section = temp.slug;
                section_name = temp.name;
                section_have = 0;
                section_shown = 0;
            }

            // Don't show filtered sets
            if (
                (!settings.alliance && (set_name.startsWith('Alliance') || section_name.startsWith('Alliance'))) ||
                (!settings.horde && (set_name.startsWith('Horde') || section_name.startsWith('Horde'))) ||
                (!settings.alliance && !settings.horde && (set_name.startsWith('Both') || section_name.startsWith('Both'))) ||
                (!settings.unavailable && (set_name.startsWith('Unavailable') || section_name.startsWith('Unavailable'))) ||
                (!settings.chaching && (set_name.startsWith('$$$$$') || section_name.startsWith('$$$$$')))
            ) {
                continue;
            }

            for (var j = 0; j < set_things.length; j++) {
                var thing = set_things[j],
                    have_thing = false;

                // Multiple items for one mount
                if (thing instanceof Array) {
                    var haveA = (settings.alliance && thing[0] && have[thing[0].id]),
                        haveH = (settings.horde && thing[1] && have[thing[1].id]);

                    if (haveA || haveH) {
                        var foo = null;
                        if (haveH === true) {
                            //haveCopy.splice(haveCopy.indexOf(thing[1].id), 1);
                            foo = thing[1];
                        }
                        if (haveA === true) {
                            //haveCopy.splice(haveCopy.indexOf(thing[0].id), 1);
                            foo = thing[0];
                        }
                        thing = foo;
                        have_thing = true;
                    }
                    else {
                        thing = (settings.alliance && thing[0] ? thing[0] : (settings.horde && thing[1] ? thing[1] : null));
                        have_thing = false;
                    }
                }
                else {
                    have_thing = (have[thing.id] !== undefined);
                }

                if (thing === null) {
                    continue;
                }

                if ((settings.collected && have_thing) || (settings.not_collected && !have_thing)) {
                    if (unique_shown.indexOf(thing.id) === -1) {
                        unique_shown.push(thing.id);
                        total_shown++;
                    }
                    section_shown++;

                    var extra = '';
                    if (hash === 'pets' && have[thing.id]) {
                        thing.quality = have[thing.id][0].quality;
                        extra = have[thing.id][0].level;
                    }

                    temp.things.push({
                        id: thing.id,
                        icon: thing.icon.split('.')[0],
                        quality: thing.quality,
                        have: have_thing,
                        extra: extra,
                    });

                    if (!have_thing) {
                        temp.have_all = false;
                    }
                    else {
                        if (unique_have.indexOf(thing.id) === -1) {
                            unique_have.push(thing.id);
                            total_have++;
                        }
                        section_have++;
                    }
                }
            }

            if (temp.things.length > 0 || temp.new_section) {
                sets.push(temp);
            }
        }

        // Finish up sections
        if (section) {
            section_data[section] = [section_have, section_shown];
        }

        // Now we can do set splitting
        var split_sets = [];
        for (var i = 0; i < sets.length; i++) {
            var set = sets[i];
            if (set.things.length >= 12) {
                var n = Math.ceil(set.things.length / 8);
                for (var j = 0; j < n; j++) {
                    var temp = {
                        have_all: true,
                        name: set.name + '(' + (j + 1) + ')',
                        things: set.things.slice(j * 8, (j * 8) + 8),
                    }
                    for (var k = 0; k < temp.things.length; k++) {
                        if (!temp.things[k].have) {
                            temp.have_all = false;
                            break;
                        }
                    }
                    split_sets.push(temp);
                }
            }
            else {
                split_sets.push(set);
            }
        }

        // Render the template
        $('#sets-inner')[0].innerHTML = templates.sets_inner({
            object: (hash === 'pets' ? 'npcs' : (hash === 'mounts' ? 'spells' : 'items')),
            iconPath: (hash === 'toys' ? 'items' : 'spells'),
            prefix: hash,
            sets: split_sets,
        });

        // Set the shown text
        $('#sets-shown')[0].innerHTML = total_have + ' / ' + total_shown;

        // Maybe hide alliance/horde things
        if (hash !== 'mounts' || (!found_alliance && !found_horde)) {
            $('#sets-alliance, #sets-horde').closest('span').hide();
        }

        // Do section stuff
        for (var k in section_data) {
            k = k.replace('/', '_');
            var have = section_data[k][0],
                shown = section_data[k][1];
            if (shown === 0) {
                $('#' + k).parent().hide();
            }
            else {
                $('#' + k + ' small').text('(' + have + '/' + shown + ')');
            }
        }

        activate_content_stuff();
    };

    ///////////////////////////////////////////////////////////////////////////
    // Progress
    var build_progress = function() {
        cache.home_content[0].innerHTML = templates.progress({
            categories: WoWthing.data.progress,
        });
        show_content();

        // Hook some events
        $('.js-progress-category').on('click', progress_category_click);

        // Check the original page hash
        if (loadHash) {
            var categoryID = loadHash.split(':')[1];
            if (categoryID === undefined) {
                $('#sub-sidebar a')[0].click();
            }
            else if (categoryID) {
                categoryID = categoryID.replace(/\W/g, '');
                $('.js-progress-category[data-category="' + categoryID + '"]').click();
            }
            loadHash = undefined;
        }
        else {
            $('#sub-sidebar a')[0].click();
        }

        activate_content_stuff();
    };

    var progress_category_click = function(e) {
        if (e.preventDefault) {
            e.preventDefault();
        }

        var $this = $(this),
            categoryID = this.getAttribute('data-category');

        // Set this link as active
        $('#sub-sidebar a').removeClass('active');
        $this.addClass('active');

        WoWthing.util.update_hash(this.href.split('#')[1]);

        shownProgressCategory = categoryID;
        render_progress(categoryID);

        activate_content_stuff();

        return false;
    };

    var render_progress = function(categoryID) {
        var progress;
        for (var i = 0; i < WoWthing.data.progress.length; i++) {
            var thing = WoWthing.data.progress[i];
            if (thing[1] === categoryID) {
                $('#progress-content')[0].innerHTML = templates.progress_section({
                    realms: realmListShow,
                    data: thing[2],
                });
                break;
            }
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Transmog
    var load_transmog = function(callback) {
        if (loaded_transmog === 0) {
            cache.home_content[0].innerHTML = '<div>Loading data, please wait...</div>';
            show_content();

            loaded_transmog = 1;

            $.cachedScript(WoWthing.home._transmog_js, {
                success: function() {
                    loaded_transmog = 2;
                    callback();
                },
            });

            return true;
        }
        else {
            return (loaded_transmog !== 2);
        }
    };

    var build_transmog = function() {
        if (load_items(build_transmog))
            return;

        if (load_transmog(build_transmog))
            return;

        // Now we can do the damn template
        cache.home_content[0].innerHTML = templates.transmog({
        });
        show_content();
        
        WoWthing.checkbox_buttons.go();

        // Bind the clicky events
        $('#transmog-subclass').on('change', function() {
            build_transmog_inner();
        });

        $('#transmog-class').on('change', function() {
            var options = [],
                val = $(this).val();

            if (val === 'Weapon') {
                options.push('Off-hand');
                for (var key in WoWthing.data.transmog.Weapon) {
                    options.push('<option value="' + key + '">' + key + '</option>');
                }
            }
            else {
                for (var k in WoWthing.data.transmog.Armor) {
                    if (k.startsWith(val)) {
                        var key = k.split(':')[1];
                        options.push('<option value="' + key + '">' + key + '</option>');
                    }
                }
            }

            options.sort();
            $('#transmog-subclass').html(options.join('')).change();
        }).change();
    };

    var build_transmog_inner = function() {
        var cls = $('#transmog-class').val(),
            subcls = $('#transmog-subclass').val();

        console.log("Building transmog for", cls, "->", subcls);

        var datas;
        if (cls === 'Weapon') {
            datas = WoWthing.data.transmog.Weapon[subcls];
        }
        else {
            datas = WoWthing.data.transmog.Armor[cls + ':' + subcls];
        }

        var have_things = [],
            other_things = [];
        for (var i in datas) {
            var data = datas[i],
                have = false;
            for (var j in data[1]) {
                var itemID = data[1][j];
                if (userItems[itemID] === true) {
                    have = true;
                    break;
                }
                else {
                    var quests = WoWthing.data.transmog_quest[itemID] || [];
                    if (quests.length > 0) {
                        for (var k in quests) {
                            if (userQuests[quests[k]] === true) {
                                have = true;
                                break;
                            }
                        }
                    }
                }
            }

            (have ? have_things : other_things).push({
                icon: data[0],
                id: data[1][0],
                have: have,
            });
        }

        // Now we can do the damn template
        $('#transmog-inner')[0].innerHTML = templates.transmog_inner({
            things: have_things.concat(other_things),
        });
    };

    ///////////////////////////////////////////////////////////////////////////
    // Settings
    var build_settings = function() {
        // Currency delete event
        $('#home-settings-currencies').on('click', '.js-currency-delete', function() {
            var currencyID = $(this).parent().data('currency');
            var index = userdata.profile.overview_show_currencies.indexOf(currencyID);
            if (index >= 0) {
                userdata.profile.overview_show_currencies.splice(index, 1);
            }
            render_settings_currencies();
        });

        // Activate the currency typeahead
        $('#home-settings-currency-search').typeahead({
            items: 10,
            source: currencySearch,
            updater: function(item) {
                for (var k in WoWthing.data.currencies) {
                    var currency = WoWthing.data.currencies[k];
                    if (currency.name === item) {
                        if (userdata.profile.overview_show_currencies.indexOf(k) === -1) {
                            userdata.profile.overview_show_currencies.push(k);
                            render_settings_currencies();
                        }
                        break;
                    }
                }
            },
        });

        // Lockout delete event
        $('#home-settings-lockouts').on('click', '.js-lockout-delete', function() {
            var instanceID = $(this).parent().data('instance');
            var index = userdata.profile.overview_show_lockouts.indexOf(instanceID);
            if (index >= 0) {
                userdata.profile.overview_show_lockouts.splice(index, 1);
            }
            render_settings_lockouts();
        });

        // Activate the lockout typeahead
        $('#home-settings-lockout-search').typeahead({
            items: 10,
            source: lockoutSearch,
            updater: function(item) {
                for (var k in WoWthing.data.instances) {
                    var instance = WoWthing.data.instances[k];
                    if (instance.name === item) {
                        if (userdata.profile.overview_show_lockouts.indexOf(k) === -1) {
                            userdata.profile.overview_show_lockouts.push(k);
                            render_settings_lockouts();
                        }
                        break;
                    }
                }
            },
        });

        // Render the template
        cache.home_settings_hide[0].innerHTML = templates.settings_hide({ realms: realmList });
        render_settings_currencies();
        render_settings_lockouts();

        show_settings();

        activate_content_stuff();
    };

    var render_settings_currencies = function() {
        // Build the currency search list
        while (currencySearch.length > 0) {
            currencySearch.pop();
        }
        for (var k in WoWthing.data.currencies) {
            if (userdata.profile.overview_show_currencies.indexOf(parseInt(k)) === -1) {
                currencySearch.push(WoWthing.data.currencies[k].name);
            }
        }
        currencySearch.sort();

        // Build the instance list
        var currencies = [];
        for (var i = 0; i < userdata.profile.overview_show_currencies.length; i++) {
            currencies.push(WoWthing.data.currencies[userdata.profile.overview_show_currencies[i]]);
        }

        // Render the template
        $('#home-settings-currencies')[0].innerHTML = templates.settings_currencies({ currencies: currencies });

        // Activate the sortable
        $('#home-settings-currencies').sortable({
            items: 'li',
            placeholder: '<li class="list-group-item"><span class="glyphicon glyphicon-arrow-right"></span></li>',
            distance: 15,
        }).bind('sortupdate', function(e, ui) {
            while (userdata.profile.overview_show_currencies.length > 0) {
                userdata.profile.overview_show_currencies.pop();
            }

            var lis = $('#home-settings-currencies li');
            for (var i = 0; i < lis.length; i++) {
                userdata.profile.overview_show_currencies.push(parseInt(lis[i].getAttribute('data-currency')));
            }
            $('#home-settings-currencies-input').val(userdata.profile.overview_show_currencies.join(','));
        });

        // Update the input
        $('#home-settings-currencies-input').val(userdata.profile.overview_show_currencies.join(','));
    };

    var render_settings_lockouts = function() {
        // Build the lockouts search list
        while (lockoutSearch.length > 0) {
            lockoutSearch.pop();
        }
        for (var k in WoWthing.data.instances) {
            if (userdata.profile.overview_show_lockouts.indexOf(parseInt(k)) === -1) {
                lockoutSearch.push(WoWthing.data.instances[k].name);
            }
        }
        lockoutSearch.sort();

        // Build the instance list
        var instances = [];
        for (var i = 0; i < userdata.profile.overview_show_lockouts.length; i++) {
            instances.push(WoWthing.data.instances[userdata.profile.overview_show_lockouts[i]]);
        }

        // Render the template
        $('#home-settings-lockouts')[0].innerHTML = templates.settings_lockouts({ instances: instances });

        // Activate the sortable
        $('#home-settings-lockouts').sortable({
            items: 'li',
            placeholder: '<li class="list-group-item"><span class="glyphicon glyphicon-arrow-right"></span></li>',
            distance: 15,
        }).bind('sortupdate', function(e, ui) {
            while (userdata.profile.overview_show_lockouts.length > 0) {
                userdata.profile.overview_show_lockouts.pop();
            }

            var lis = $('#home-settings-lockouts li');
            for (var i = 0; i < lis.length; i++) {
                userdata.profile.overview_show_lockouts.push(parseInt(lis[i].getAttribute('data-instance')));
            }
            $('#home-settings-lockouts-input').val(userdata.profile.overview_show_lockouts.join(','));
        });

        // Update the input
        $('#home-settings-lockouts-input').val(userdata.profile.overview_show_lockouts.join(','));
    };

    ///////////////////////////////////////////////////////////////////////////

    var overview_character_sort = function(a, b) {
        for (var i = 0; i < sortOrder.length; i++) {
            var result = sort_internal(sortOrder[i][0], a, b);
            if (result !== 0) {
                return sortOrder[i][1] ? -result : result;
            }
        }
        return 0;
    };

    var sort_internal = function(thing, a, b) {
        if (thing === 'realm') {
            if (a.realm < b.realm) { return -1; }
            else if (a.realm > b.realm) { return 1; }
            return 0;
        }
        if (thing === 'name') {
            if (a.name < b.name) { return -1; }
            else if (a.name > b.name) { return 1; }
            return 0;
        }
        else if (thing === 'level') {
            if (a.level < b.level) { return -1; }
            else if (a.level > b.level) { return 1; }

            if (a.current_xp < b.current_xp) { return -1; }
            else if (a.current_xp > b.current_xp) { return 1; }

            return 0;
        }
        else if (thing === 'itemlevel') {
            var aLvl = parseFloat(a.average_ilvl),
                bLvl = parseFloat(b.average_ilvl);
            if (aLvl < bLvl) { return -1; }
            else if (aLvl > bLvl) { return 1; }
            return 0;
        }
    };

    ///////////////////////////////////////////////////////////////////////////

    var lockout_popover = function(lockout) {
        var popover = '<table class=\'table lockout-tooltip\'>';
        if (lockout && lockout.bosses.length > 0) {
            for (var j = 0; j < lockout.bosses.length; j++) {
                var boss = lockout.bosses[j];
                popover += '<tr><td>' + boss[0] + '</td>';
                if (boss[1]) {
                    popover += '<td class=\'lockout-defeated\'>Defeated</td>';
                }
                else {
                    popover += '<td class=\'lockout-available\'>Available</td>';
                }
                popover += '</tr>'
            }
        }

        popover += '<tr class=\'lockout-expires\'><td colspan=2>';
        if (lockout === null) {
            popover += 'Not done this week!';
        }
        else if (lockout.expires === null) {
            popover += 'Expired!';
        }
        else {
            popover += 'Expires: ' + lockout.expires.format("ddd YYYY-MM-DD HH:mm");
        }
        popover += '</td></tr></table>';

        return popover;
    };

    var show_content = function() {
        if (contentShown) {
            return;
        }
        cache.home_content.show();
        cache.home_settings.hide();
        contentShown = true;
        settingsShown = false;
    }

    var show_settings = function() {
        if (settingsShown) {
            return;
        }
        cache.home_content.hide();
        cache.home_settings.show();
        contentShown = false;
        settingsShown = true;
    }

    var activate_content_stuff = function() {
        setTimeout(function() {
            // Tooltips
            $('[rel="tooltip"]', cache.home_content).tooltip({
                container: 'body',
                html: true,
            });
        }, 1);

        setTimeout(function() {
            // Tippy
            tippy('[data-tippy-content]', {
                //arrow: true,
                distance: 2,
                duration: 0,
                //size: 'large',
            });
        }, 1);
        
        setTimeout(function() {
            // Popovers
            $('[rel="popover"]', cache.home_content).popover({
                animation: false,
                container: 'body',
                html: true,
                placement: 'auto right',
                trigger: 'hover',
                sanitizeFn: function (content) {
                    return content
                },
            });
        }, 1);

        setTimeout(function() {
            // WoWDB tooltips
            if (window.__tip) {
                __tip.watchElligibleElements();
            }
        }, 1);
    };

    var CharacterCurrency = function(obj) {
        this.id = obj[0];
        this.have_value = obj[1];
        this.have_max = obj[2] || 0;
        this.weekly_value = obj[3] || 0;
        this.weekly_max = obj[4] || 0;
    }

    var CharacterPet = function(obj) {
        this.id = obj.i;
        this.favourite = obj.f;
        this.level = obj.l;
        this.name = obj.n;
        this.quality = obj.q;
    }

    var CharacterReputation = function(obj) {
        this.faction_id = obj[0];
        this.level = obj[1];
        this.reputation_value = obj[2];
        this.reputation_max = obj[3];
        this.paragon_value = obj[4];
        this.paragon_max = obj[5];
        this.paragon_ready = obj[6];

        this.reputation_current = 0;

        // Fix Blizzard's stupid API return values for bodyguards
        if (WoWthing.data.bodyguard_factions.indexOf(this.faction_id) >= 0) {
            var actualValue = this.reputation_value;
            if (this.level === 5) {
                actualValue += 3000 + 6000;
            }
            else if (this.level === 4) {
                actualValue += 3000;
            }

            if (actualValue >= 20000) {
                this.level = 2;
                this.reputation_value = 0;
                this.reputation_max = 0;
            }
            else if (actualValue >= 10000) {
                this.level = 1;
                this.reputation_value = actualValue - 10000;
                this.reputation_max = 10000;
            }
            else {
                this.level = 0;
                this.reputation_value = actualValue;
                this.reputation_max = 10000;
            }
        }
        else if (this.level >= 3) {
            this.reputation_current = this.reputation_value;
            if (this.level === 7) {
                this.reputation_current += 42000;
            }
            else if (this.level === 6) {
                this.reputation_current += 21000;
            }
            else if (this.level === 5) {
                this.reputation_current += 9000;
            }
            else if (this.level === 4) {
                this.reputation_current += 3000;
            }
        }
    }
    CharacterReputation.prototype.getLevelName = function(rep) {
        return WoWthing.data.reputation_levels[rep.bodyguard === true ? 'bodyguard' : (rep.friend === true ? 'friend' : 'reputation')][this.level];
    }

    var Item = function(obj) {
        this.i = obj[0];
        this.q = obj[1];
        this.u = obj[2];
    }

    var build = {
        overview: build_overview,
        achievements: build_achievements,
        currencies: build_currencies,
        gear: build_gear,
        gold_history: build_gold_history,
        items: build_items,
        lockouts: build_lockouts,
        mounts: build_mounts,
        pets: build_pets,
        progress: build_progress,
        reputations: build_reputations,
        toys: build_toys,
        transmog: build_transmog,
        settings: build_settings,
    };

    // Public stuff
    return {
        activate_content_stuff: activate_content_stuff,
        onload: onload,
    };
})();
