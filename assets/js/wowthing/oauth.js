var WoWthing = WoWthing || {};
WoWthing.oauth = (function() {
    var onload = function() {
        $('#oauth-region').on('change', function() {
            $('#oauth-link').attr('href', '/oauth/begin/?region=' + this.value);
        }).change();
    };

    // Public stuff
    return {
        onload: onload,
    };
})();
