import utils from '../utils.js';

export const store = new Vuex.Store({
    state: {
        loading: true,
        public: false,
        quests: {},
        totalGold: 0,
        totalPlayed: 0,
    },
    actions: {
        loadUserdata ({ commit }) {
            $.ajax({
                url: WoWthingState.userdata_url,
                success: function(data, textStatus, jqXHR) {
                    console.time('loadUserdata_success');

                    let finalData = {
                        quests: {},
                        totalGold: 0,
                        totalPlayed: 0,
                    };

                    // Grouping stuff
                    let overviewGrouping = data.profile.overview_group.replace('default', 'realm').split(',');

                    // Sort order stuff
                    let sortTemp = data.profile.overview_sort.replace('default', '-level,name').split(','),
                        sortOrder = [];
                    for (let thing of sortTemp) {
                        var invert = false;
                        if (thing.startsWith('-')) {
                            thing = thing.replace('-', '');
                            invert = true;
                        }
                        sortOrder.push([thing, invert]);
                    }

                    // Iterate over accounts doing stuff
                    for (let account of data.accounts) {
                        let quests = utils.decode_base64_uint16_array(account.quests);
                        for (let questID of quests) {
                            finalData.quests[questID] = true;
                        }
                    }

                    console.timeEnd('loadUserdata_success');
                    console.log(data);

                    commit('setData', finalData);
                    commit('setLoading', false);
                },
            });
        },
    },
    mutations: {
        setLoading (state, value) {
            state.loading = value;
        },
        setData (state, value) {
            for (let key in value) {
                state[key] = value[key];
            }
            console.log(state);
        },
    },
});
