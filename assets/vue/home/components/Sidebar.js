export default {
    name: 'sidebar',
    template: `
<div class="col-sidebar" id="home-sidebar">
    <div class="list-group">
        <router-link to="overview" active-class="active" class="list-group-item list-group-item-action">Overview</router-link>
    </div>

    <div class="list-group">
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
        <a class="list-group-item list-group-item-action">BROKEN</a>
    </div>
</div>
`,
}

let foo = `    <div class="list-group">
      <a href="#overview" class="list-group-item js-sidebar">Overview</a>
      <a href="#achievements" class="list-group-item js-sidebar">Achievements<span id="badge-achievements" class="badge"></span></a>
{% if not public %}
      <a href="#currencies" class="list-group-item js-sidebar">Currencies</a>
{% endif %}
      <a href="#gear" class="list-group-item js-sidebar">Gear</a>
{% if not public %}
      <a href="#gold_history" class="list-group-item js-sidebar">Gold history</a>
      <a href="#items" class="list-group-item js-sidebar">Items</a>
      <a href="#lockouts" class="list-group-item js-sidebar">Lockouts<span id="badge-lockouts" class="badge"></span></a>
{% endif %}
      <a href="#order_halls" class="list-group-item js-sidebar">Order Halls</a>
      <a href="#progress" class="list-group-item js-sidebar">Progress</a>
      <a href="#reputations" class="list-group-item js-sidebar">Reputations</a>
    </div>

    <div class="list-group">
      <a href="#mounts" class="list-group-item js-sidebar">Mounts<span id="badge-mounts" class="badge"></span></a>
      <a href="#pets" class="list-group-item js-sidebar">Pets<span id="badge-pets" class="badge"></span></a>
      <a href="#toys" class="list-group-item js-sidebar">Toys<span id="badge-toys" class="badge"></span></a>
    </div>

{% if not public %}
    <div class="list-group">
      <!--<a href="#transmog" class="list-group-item js-sidebar">Transmog<span class="badge">WIP</span></a>-->
      <a href="#settings" class="list-group-item js-sidebar">Settings</a>
    </div>
{% endif %}
`;
