export default {
    decode_base64_uint16_array (s) {
        var decoded = atob(s),
            ret = [];
        for (var i = 0; i < decoded.length; i += 2) {
            ret.push((decoded.charCodeAt(i)) | (decoded.charCodeAt(i + 1) << 8));
        }
        return ret;
    },
}
